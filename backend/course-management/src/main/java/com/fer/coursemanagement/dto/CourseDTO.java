package com.fer.coursemanagement.dto;

import com.fer.coursemanagement.domain.Course;

import java.util.ArrayList;
import java.util.List;

public class CourseDTO {

    private Long id;
    private String name;
    private String description;
    private Long lecturerId;
    private double price;
    private int capacity;
    private int freeSeats;
    private int noOfLectures;
    List<Long> membersId;

    public CourseDTO(){
    }

    public CourseDTO(Course course){
        this.id=course.getId();
        this.name=course.getName();
        this.description=course.getDescription();
        this.price=course.getPrice();
        this.capacity=course.getCapacity();
        this.freeSeats=course.getFreeSeats();
        this.noOfLectures=course.getNoOfLectures();
        this.membersId=new ArrayList<>();


        if(course.getLecturer()!=null){
            this.lecturerId=course.getLecturer().getId();
        }

        if(course.getMembers()!=null){
            course.getMembers().forEach(m->this.membersId.add(m.getId()));
        }

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getLecturerId() {
        return lecturerId;
    }

    public void setLecturerId(Long lecturerId) {
        this.lecturerId = lecturerId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getFreeSeats() {
        return freeSeats;
    }

    public void setFreeSeats(int freeSeats) {
        this.freeSeats = freeSeats;
    }

    public int getNoOfLectures() {
        return noOfLectures;
    }

    public void setNoOfLectures(int noOfLectures) {
        this.noOfLectures = noOfLectures;
    }

    public List<Long> getMembersId() {
        return membersId;
    }

    public void setMembersId(List<Long> membersId) {
        this.membersId = membersId;
    }
}
