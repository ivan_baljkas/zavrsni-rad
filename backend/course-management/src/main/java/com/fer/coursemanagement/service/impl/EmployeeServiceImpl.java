package com.fer.coursemanagement.service.impl;

import com.fer.coursemanagement.dao.EmployeeRepository;
import com.fer.coursemanagement.domain.Employee;
import com.fer.coursemanagement.dto.EmployeeDTO;
import com.fer.coursemanagement.service.CourseService;
import com.fer.coursemanagement.service.EmployeeService;
import com.fer.coursemanagement.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private RoleService roleService;

    @Autowired
    private CourseService courseService;


    @Override
    public Employee get(long employeeId) {
        return employeeRepository.findById(employeeId)
                .orElseThrow(()->new NullPointerException());
    }

    @Override
    public List<EmployeeDTO> listAll() {
        List<EmployeeDTO> list = new ArrayList<>();
        employeeRepository.findAll().forEach(e->list.add(new EmployeeDTO(e)));
        return list;
    }

    @Override
    public EmployeeDTO findById(long employeeId) {
        Optional<Employee> employee=employeeRepository.findById(employeeId);

        if(!employee.isPresent()){
            throw new NullPointerException();
        }

        return new EmployeeDTO(employee.get());
    }

    @Override
    public EmployeeDTO findByUsername(String username) {
        Optional<Employee> employee=employeeRepository.findByUsername(username);

        if(!employee.isPresent()){
            throw new NullPointerException();
        }

        return new EmployeeDTO(employee.get());
    }

    @Override
    public EmployeeDTO createEmployee(EmployeeDTO dto) {
        validate(dto);
        Assert.isNull(dto.getId(), "Employee ID must be null, not: " + dto.getId());

        if(employeeRepository.countByUsername(dto.getUsername())>0){
            throw new IllegalArgumentException();
        }

        return new EmployeeDTO(employeeRepository.save(employeeFromDTO(dto)));

    }

    @Override
    public EmployeeDTO updateEmployee(EmployeeDTO dto) {
        validate(dto);
        if(!employeeRepository.existsById(dto.getId())){
            throw new IllegalArgumentException();
        }
        if(employeeRepository.existsByUsernameAndIdNot(dto.getUsername(),dto.getId())){
            throw new IllegalArgumentException();
        }

        return new EmployeeDTO(employeeRepository.save(employeeFromDTO(dto)));
    }

    @Override
    public EmployeeDTO deleteEmployee(long employeeId) {
        EmployeeDTO dto = findById(employeeId);

        courseService.listAll().stream().filter(c->c.getLecturerId()==employeeId)
                .forEach(c->{
                    c.setLecturerId(null);
                    courseService.updateCourse(c);
                });

        employeeRepository.delete(employeeFromDTO(dto));
        return dto;
    }

    public Employee employeeFromDTO(EmployeeDTO dto) {
        Employee employee = new Employee();

        employee.setId(dto.getId());
        employee.setUsername(dto.getUsername());
        employee.setPassword(dto.getPassword());
        employee.setFirstName(dto.getFirstName());
        employee.setLastName(dto.getLastName());
        employee.setEmail(dto.getEmail());
        employee.setDateOfBirth(dto.getDateOfBirth());


        if (dto.getRoleId() != null) {
            employee.setRole(roleService.get(dto.getRoleId()));
        }

        return employee;
    }


    private void validate(EmployeeDTO dto) {
        Assert.notNull(dto, "Employee object must be given");
        Assert.hasText(dto.getUsername(), "Username must be given");
        Assert.hasText(dto.getEmail(), "Email must be given");
        Assert.hasText(dto.getFirstName(), "First name must be given");
        Assert.hasText(dto.getLastName(), "Last name must be given");
        Assert.notNull(dto.getDateOfBirth(), "Date of birth must be given");
    }
}
