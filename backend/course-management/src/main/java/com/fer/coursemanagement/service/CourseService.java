package com.fer.coursemanagement.service;

import com.fer.coursemanagement.domain.Course;
import com.fer.coursemanagement.dto.CourseDTO;

import java.util.List;

public interface CourseService {

    Course get(long courseId);

    List<CourseDTO> listAll();

    CourseDTO findById(long employeeId);

    CourseDTO findByName(String name);

    CourseDTO createCourse(CourseDTO dto);

    CourseDTO updateCourse(CourseDTO dto);

    CourseDTO deleteCourse(long courseId);
}
