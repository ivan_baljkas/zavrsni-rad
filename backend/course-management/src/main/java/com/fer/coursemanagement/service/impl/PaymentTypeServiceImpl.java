package com.fer.coursemanagement.service.impl;

import com.fer.coursemanagement.dao.PaymentTypeRepository;
import com.fer.coursemanagement.domain.PaymentType;
import com.fer.coursemanagement.service.PaymentService;
import com.fer.coursemanagement.service.PaymentTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Optional;

@Service
public class PaymentTypeServiceImpl implements PaymentTypeService {

    @Autowired
    private PaymentTypeRepository paymentTypeRepository;

    @Autowired
    private PaymentService paymentService;

    @Override
    public List<PaymentType> listAll() {
        return paymentTypeRepository.findAll();
    }

    @Override
    public Long getByName(String name){
      return  paymentTypeRepository.findAll().stream()
        .filter(p->p.getName().equals(name))
        .findFirst().get().getId();
    }

    @Override
    public PaymentType get(long paymentTypeId) {
        return paymentTypeRepository.findById(paymentTypeId)
                .orElseThrow(()->new NullPointerException());
    }

    @Override
    public PaymentType createPaymentType(PaymentType paymentType) {
        validate(paymentType);
        Assert.isNull(paymentType.getId(), "PaymentType ID must be null, not: " + paymentType.getId());

        if(paymentTypeRepository.countByName(paymentType.getName())>0){
            throw new IllegalArgumentException();
        }

        return paymentTypeRepository.save(paymentType);
    }

    @Override
    public PaymentType updatePaymentType(PaymentType paymentType) {
        validate(paymentType);
        long paymentTypeId = paymentType.getId();

        if(!paymentTypeRepository.existsById(paymentTypeId)){
            throw new IllegalArgumentException();
        }

        if(paymentTypeRepository.existsByNameAndIdNot(paymentType.getName(),paymentTypeId)){
            throw new IllegalArgumentException();
        }

        return paymentTypeRepository.save(paymentType);
    }

    @Override
    public PaymentType deletePaymentType(long paymentTypeId) {
        PaymentType paymentType = get(paymentTypeId);

        paymentService.listAll().stream().filter(p->p.getPaymentTypeId().equals(paymentTypeId))
                .forEach(p->{
                    p.setPaymentTypeId(null);
                    paymentService.updatePayment(p);
                });

        paymentTypeRepository.delete(paymentType);
        return paymentType;
    }

    private void validate(PaymentType paymentType) {
        Assert.notNull(paymentType, "PaymentType object must be given");
        Assert.hasText(paymentType.getName(), "PaymentType name must be given");
    }
}
