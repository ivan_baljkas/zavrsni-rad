package com.fer.coursemanagement.service.impl;

import com.fer.coursemanagement.dao.PaymentRepository;
import com.fer.coursemanagement.domain.Payment;
import com.fer.coursemanagement.dto.PaymentDTO;
import com.fer.coursemanagement.service.CourseService;
import com.fer.coursemanagement.service.CustomerService;
import com.fer.coursemanagement.service.PaymentService;
import com.fer.coursemanagement.service.PaymentTypeService;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private CourseService courseService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private PaymentTypeService paymentTypeService;

    @Autowired
    private KieSession session;

    @Override
    public Payment get(long paymentId) {
        return paymentRepository.findById(paymentId)
                .orElseThrow(()->new NullPointerException());
    }

    @Override
    public List<PaymentDTO> listAll() {
        List<PaymentDTO> list = new ArrayList<>();
        paymentRepository.findAll().forEach(p->list.add(new PaymentDTO(p)));
        return list;
    }

    @Override
    public PaymentDTO findById(long paymentId) {
        Optional<Payment> payment = paymentRepository.findById(paymentId);

        if(!payment.isPresent()){
            throw new NullPointerException();
        }

        return new PaymentDTO(payment.get());
    }

    @Override
    public PaymentDTO createPayment(PaymentDTO dto) {
        validate(dto);
        Assert.isNull(dto.getId(), "Payment ID must be null, not: " + dto.getId());

        session.setGlobal("cashId",paymentTypeService.getByName("CASH"));
        session.insert(dto);
        session.fireAllRules();

        return new PaymentDTO(paymentRepository.save(paymentFromDTO(dto)));
    }

    @Override
    public PaymentDTO updatePayment(PaymentDTO dto) {
        validate(dto);

        if(!paymentRepository.existsById(dto.getId())){
            throw new IllegalArgumentException();
        }

        session.setGlobal("cashId",paymentTypeService.getByName("CASH"));
        session.insert(dto);
        session.fireAllRules();

        return new PaymentDTO(paymentRepository.save(paymentFromDTO(dto)));
    }

    @Override
    public PaymentDTO deletePayment(long paymentId) {
        PaymentDTO dto = findById(paymentId);

        paymentRepository.delete(paymentFromDTO(dto));

        return dto;
    }

    public Payment paymentFromDTO(PaymentDTO dto){
        Payment payment = new Payment();

        payment.setId(dto.getId());
        payment.setPrice(dto.getPrice());
        payment.setDiscount(dto.getDiscount());

        if(dto.getCourseId()!=null){
            payment.setCourse(courseService.get(dto.getCourseId()));
        }

        if(dto.getCustomerId()!=null){
            payment.setCustomer(customerService.get(dto.getCustomerId()));
        }

        if(dto.getPaymentTypeId()!=null){
            payment.setPaymentType(paymentTypeService.get(dto.getPaymentTypeId()));
        }

        return payment;
    }

    private void validate(PaymentDTO dto) {
        Assert.notNull(dto, "Payment object must be given");
        Assert.notNull(dto.getPrice(), "Price must be given");
    }
}
