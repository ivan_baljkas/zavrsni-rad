package com.fer.coursemanagement.service;

import com.fer.coursemanagement.domain.PaymentType;

import java.util.List;

public interface PaymentTypeService {

    List<PaymentType> listAll();

    Long getByName(String name);

    PaymentType get(long paymentTypeId);

    PaymentType createPaymentType(PaymentType paymentType);

    PaymentType updatePaymentType(PaymentType paymentType);

    PaymentType deletePaymentType(long paymentTypeId);
}
