package com.fer.coursemanagement.domain;

import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.util.List;

@Entity
public class Course {

    @Id
    @GeneratedValue
    private Long id;

    @NonNull
    @Column(unique = true)
    private String name;

    private String description;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "lecturer_id")
    private Employee lecturer;

    @NonNull
    private int capacity;

    @NonNull
    private int freeSeats;

    @NonNull
    private double price;

    @NonNull
    private int noOfLectures;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "course_members", joinColumns = @JoinColumn(name = "course_id"), inverseJoinColumns = @JoinColumn(name = "customer_id"))
    private List<Customer> members;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Employee getLecturer() {
        return lecturer;
    }

    public void setLecturer(Employee lecturer) {
        this.lecturer = lecturer;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getFreeSeats() {
        return freeSeats;
    }

    public void setFreeSeats(int freeSeats) {
        this.freeSeats = freeSeats;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getNoOfLectures() {
        return noOfLectures;
    }

    public void setNoOfLectures(int noOfLectures) {
        noOfLectures = noOfLectures;
    }

    public List<Customer> getMembers() {
        return members;
    }

    public void setMembers(List<Customer> members) {
        this.members = members;
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", lecturer=" + lecturer +
                ", capacity=" + capacity +
                ", freeSeats=" + freeSeats +
                ", price=" + price +
                ", noOfLectures=" + noOfLectures +
                ", members=" + members +
                '}';
    }
}
