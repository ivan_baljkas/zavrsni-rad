package com.fer.coursemanagement.rest;

import com.fer.coursemanagement.dto.EmployeeDTO;
import com.fer.coursemanagement.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/employees")
public class EmployeeController {

      @Autowired
    private EmployeeService employeeService;

    @GetMapping
    public List<EmployeeDTO> listAll(){
        return employeeService.listAll();
    }

    @GetMapping(path="/{id}")
    public EmployeeDTO getEmployee(@PathVariable("id") long id){
        return employeeService.findById(id);
    }

    @PostMapping
    public EmployeeDTO createEmployee(@RequestBody EmployeeDTO dto){
        EmployeeDTO saved=employeeService.createEmployee(dto);
        return saved;
    }

    @PutMapping(path = "/{id}")
    public EmployeeDTO updateEmployee(@PathVariable("id") Long id, @RequestBody EmployeeDTO dto){
        if(!dto.getId().equals(id)){
            throw new IllegalArgumentException();
        }
        return employeeService.updateEmployee(dto);
    }

    @DeleteMapping(path = "/{id}")
    public EmployeeDTO deleteEmployee(@PathVariable("id") Long id){
        return employeeService.deleteEmployee(id);
    }
}
