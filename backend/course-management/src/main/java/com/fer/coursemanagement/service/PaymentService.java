package com.fer.coursemanagement.service;

import com.fer.coursemanagement.domain.Payment;
import com.fer.coursemanagement.dto.PaymentDTO;

import java.util.List;

public interface PaymentService {

    Payment get(long paymentId);

    List<PaymentDTO> listAll();

    PaymentDTO findById(long paymentId);

    PaymentDTO createPayment(PaymentDTO dto);

    PaymentDTO updatePayment(PaymentDTO dto);

    PaymentDTO deletePayment(long paymentId);
}
