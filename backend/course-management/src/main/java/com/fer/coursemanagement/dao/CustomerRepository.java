package com.fer.coursemanagement.dao;

import com.fer.coursemanagement.domain.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CustomerRepository extends JpaRepository<Customer, Long> {

        int countByUsername(String username);
        boolean existsByUsernameAndIdNot(String username, Long id);

        Optional<Customer> findByUsername(String username);

        Optional<Customer> findByEmail(String email);

}
