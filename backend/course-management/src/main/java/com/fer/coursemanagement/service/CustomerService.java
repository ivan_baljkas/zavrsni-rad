package com.fer.coursemanagement.service;

import com.fer.coursemanagement.domain.Customer;

import java.util.List;

public interface CustomerService {

    Customer get(long customerId);

    List<Customer> listAll();

    Customer findByUsername(String username);

    Customer createCustomer(Customer dto);

    Customer updateCustomer(Customer dto);

    Customer deleteCustomer(long customerId);
}
