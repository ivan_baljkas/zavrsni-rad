package com.fer.coursemanagement.rest;

import com.fer.coursemanagement.dto.CourseDTO;
import com.fer.coursemanagement.service.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/courses")
public class CourseController {

    @Autowired
    private CourseService courseService;

    @GetMapping
    public List<CourseDTO> listAll(){
        return courseService.listAll();
    }

    @GetMapping(path="/{id}")
    public CourseDTO getCourse(@PathVariable("id") long id){
        return courseService.findById(id);
    }

    @PostMapping
    public CourseDTO createCourse(@RequestBody CourseDTO dto){
        CourseDTO saved=courseService.createCourse(dto);
        return saved;
    }

    @PutMapping(path = "/{id}")
    public CourseDTO updateCourse(@PathVariable("id") Long id, @RequestBody CourseDTO dto){
        if(!dto.getId().equals(id)){
            throw new IllegalArgumentException();
        }
        return courseService.updateCourse(dto);
    }

    @DeleteMapping(path = "/{id}")
    public CourseDTO deleteCourse(@PathVariable("id") Long id){
        return courseService.deleteCourse(id);
    }
}
