package com.fer.coursemanagement.rest;

import com.fer.coursemanagement.dto.PaymentDTO;
import com.fer.coursemanagement.service.PaymentService;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/payments")
public class PaymentController {

    @Autowired
    private PaymentService  paymentService;

    @GetMapping
    public List<PaymentDTO> listAll(){
        return  paymentService.listAll();
    }

    @GetMapping(path="/{id}")
    public  PaymentDTO getPayment(@PathVariable("id") long id){
        return  paymentService.findById(id);
    }

    @PostMapping
    public  PaymentDTO createPayment(@RequestBody  PaymentDTO dto){

         PaymentDTO saved= paymentService.createPayment(dto);
        return saved;
    }

    @PutMapping(path = "/{id}")
    public PaymentDTO updatePayment(@PathVariable("id") Long id, @RequestBody PaymentDTO dto){
        if(!dto.getId().equals(id)){
            throw new IllegalArgumentException();
        }
        return paymentService.updatePayment(dto);
    }

    @DeleteMapping(path = "/{id}")
    public PaymentDTO deletePayment(@PathVariable("id") Long id){
        return paymentService.deletePayment(id);
    }
}
