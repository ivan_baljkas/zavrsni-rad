package com.fer.coursemanagement.service;

import com.fer.coursemanagement.domain.Lecture;
import com.fer.coursemanagement.dto.LectureDTO;

import java.util.List;

public interface LectureService {

    Lecture get(long lectureId);

    List<LectureDTO> listAll();

    LectureDTO findById(long lectureId);

    LectureDTO createLecture(LectureDTO dto);

    LectureDTO updateLecture(LectureDTO dto);

    LectureDTO deleteLecture(long lectureId);
}
