package com.fer.coursemanagement.rest;

import com.fer.coursemanagement.dto.LectureDTO;
import com.fer.coursemanagement.service.LectureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/lectures")
public class LectureController {

    @Autowired
    LectureService lectureService;

    @GetMapping
    public List<LectureDTO> listAll(){
        return lectureService.listAll();
    }

    @GetMapping(path="/{id}")
    public LectureDTO getLecture(@PathVariable("id") long id){
        return lectureService.findById(id);
    }

    @PostMapping
    public LectureDTO createLecture(@RequestBody LectureDTO dto){
        LectureDTO saved=lectureService.createLecture(dto);
        return saved;
    }

    @PutMapping(path = "/{id}")
    public LectureDTO updateLecture(@PathVariable("id") Long id, @RequestBody LectureDTO dto){
        if(!dto.getId().equals(id)){
            throw new IllegalArgumentException();
        }
        return lectureService.updateLecture(dto);
    }

    @DeleteMapping(path = "/{id}")
    public LectureDTO deleteLecture(@PathVariable("id") Long id){
        return lectureService.deleteLecture(id);
    }
}
