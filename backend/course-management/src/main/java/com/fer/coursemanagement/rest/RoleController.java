package com.fer.coursemanagement.rest;

import com.fer.coursemanagement.domain.Role;
import com.fer.coursemanagement.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/roles")
public class RoleController {

    @Autowired
    RoleService roleService;

    @GetMapping
    public List<Role> listAll(){
        return roleService.listAll();
    }

    @GetMapping(path="/{id}")
    public Role getRole(@PathVariable("id") long id){
        return roleService.get(id);
    }

    @PostMapping
    public Role createRole(@RequestBody Role role){
        return roleService.createRole(role);
    }

    @PutMapping(path = "/{id}")
    public Role updateRole(@PathVariable("id") Long id, @RequestBody Role role){
        if(!role.getId().equals(id)){
            throw new IllegalArgumentException();
        }
        return roleService.updateRole(role);
    }

    @DeleteMapping(path = "/{id}")
    public Role deleteRole(@PathVariable("id") Long id) {
        return roleService.deleteRole(id);
    }

}
