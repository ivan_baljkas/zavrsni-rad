package com.fer.coursemanagement.dao;

import com.fer.coursemanagement.domain.Lecture;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface LectureRepository extends JpaRepository<Lecture, Long> {

    Optional<Lecture> findById(Long id);

    int countById(Long id);

    boolean existsById(Long id);
}
