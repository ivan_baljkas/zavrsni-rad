package com.fer.coursemanagement.service;

import com.fer.coursemanagement.domain.Employee;
import com.fer.coursemanagement.dto.EmployeeDTO;

import java.util.List;

public interface EmployeeService {

    Employee get(long employeeId);

    List<EmployeeDTO> listAll();

    EmployeeDTO findById(long employeeId);

    EmployeeDTO findByUsername(String username);

    EmployeeDTO createEmployee(EmployeeDTO dto);

    EmployeeDTO updateEmployee(EmployeeDTO dto);

    EmployeeDTO deleteEmployee(long employeeId);
}
