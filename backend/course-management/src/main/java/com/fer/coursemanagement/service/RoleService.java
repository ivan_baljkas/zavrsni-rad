package com.fer.coursemanagement.service;

import com.fer.coursemanagement.domain.Role;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface RoleService {
    List<Role> listAll();

    Role get(long roleId);

    Role createRole(Role role);

    Role updateRole(Role role);

    Role deleteRole(long roleId);
}
