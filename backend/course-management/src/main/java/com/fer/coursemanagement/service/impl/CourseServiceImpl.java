package com.fer.coursemanagement.service.impl;

import com.fer.coursemanagement.dao.CourseRepository;
import com.fer.coursemanagement.domain.Course;
import com.fer.coursemanagement.domain.Customer;
import com.fer.coursemanagement.dto.CourseDTO;
import com.fer.coursemanagement.service.*;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CourseServiceImpl implements CourseService {

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private LectureService lectureService;

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private KieSession session;


    @Override
    public Course get(long courseId) {
        return courseRepository.findById(courseId)
                .orElseThrow(()->new NullPointerException());
    }

    public int countCoursesWithSameLecturer(CourseDTO dto){
        return (int)courseRepository.findAll()
                .stream()
                .filter(c->c.getLecturer().getId().equals(dto.getLecturerId()))
                .count();
    }

    @Override
    public List<CourseDTO> listAll() {
        List<CourseDTO> allCourses = new ArrayList<>();
        courseRepository.findAll().forEach(c->allCourses.add(new CourseDTO(c)));
        return allCourses;
    }

    @Override
    public CourseDTO findById(long courseId) {
        Optional<Course> course = courseRepository.findById(courseId);

        if(!course.isPresent()){
            throw new NullPointerException();
        }

        return new CourseDTO(course.get());
    }

    @Override
    public CourseDTO findByName(String name) {
        Optional<Course> course = courseRepository.findByName(name);

        if(!course.isPresent()){
            throw new NullPointerException();
        }

        return new CourseDTO(course.get());
    }

    @Override
    public CourseDTO createCourse(CourseDTO dto) {
        validate(dto);
        Assert.isNull(dto.getId(), "Course ID must be null, not: " + dto.getId());

        if(courseRepository.countByName(dto.getName())>0){
            throw new IllegalArgumentException();
        }

        session.setGlobal("freeSeatsBefore",dto.getFreeSeats());
        session.insert(dto);
        session.fireAllRules();


        return new CourseDTO(courseRepository.save(courseFromDTO(dto)));
    }

    @Override
    public CourseDTO updateCourse(CourseDTO dto) {
        validate(dto);

        if(!courseRepository.existsById(dto.getId())){
            throw new IllegalArgumentException();
        }

        if(courseRepository.existsByNameAndIdNot(dto.getName(),dto.getId())){
            throw new IllegalArgumentException();
        }

        session.setGlobal("freeSeatsBefore",dto.getFreeSeats());
        session.insert(dto);
        session.fireAllRules();

        return new CourseDTO(courseRepository.save(courseFromDTO(dto)));
    }

    @Override
    public CourseDTO deleteCourse(long courseId) {
        CourseDTO dto = findById(courseId);

        lectureService.listAll().stream().filter(l->l.getCourseId()==courseId)
                .forEach(l->{
                    l.setCourseId(null);
                    lectureService.updateLecture(l);
                });

        paymentService.listAll().stream().filter(p->p.getCourseId().equals(courseId))
                .forEach(p->{
                    p.setCourseId(null);
                    paymentService.updatePayment(p);
                });

        courseRepository.delete(courseFromDTO(dto));

        return dto;
    }

    public Course courseFromDTO(CourseDTO dto) {
        Course course = new Course();

        course.setId(dto.getId());
        course.setName(dto.getName());
        course.setDescription(dto.getDescription());
        course.setCapacity(dto.getCapacity());
        course.setFreeSeats(dto.getFreeSeats());
        course.setPrice(dto.getPrice());
        course.setNoOfLectures(dto.getNoOfLectures());

        if(dto.getLecturerId()!=null){
            course.setLecturer((employeeService.get(dto.getLecturerId())));
        }


        List<Customer> members = new ArrayList<>();
        dto.getMembersId().forEach(m->members.add(customerService.get(m)));
        course.setMembers(members);

        return course;
    }


    private void validate(CourseDTO dto) {
        Assert.notNull(dto, "Course object must be given");
        Assert.hasText(dto.getName(), "Name must be given");
        Assert.hasText(dto.getDescription(), "Description must be given");
        Assert.notNull(dto.getCapacity(), "Capacity must be given");
        Assert.notNull(dto.getFreeSeats(), "Free Seats must be given");
        Assert.notNull(dto.getPrice(), "Price must be given");
        Assert.notNull(dto.getNoOfLectures(), "Number Of Lectures must be given");
    }
}
