package com.fer.coursemanagement.rest;

import com.fer.coursemanagement.domain.Customer;
import com.fer.coursemanagement.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/customers")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @GetMapping
    public List<Customer> listAll(){
        return customerService.listAll();
    }

    @GetMapping(path="/{id}")
    public Customer getCustomer(@PathVariable("id") long id){
        return customerService.get(id);
    }

    @PostMapping
    public Customer createCustomer(@RequestBody Customer customer){
        return customerService.createCustomer(customer);
    }

    @PutMapping(path = "/{id}")
    public Customer updateCustomer(@PathVariable("id") Long id, @RequestBody Customer customer){
        if(!customer.getId().equals(id)){
            throw new IllegalArgumentException();
        }
        return customerService.updateCustomer(customer);
    }

    @DeleteMapping(path = "/{id}")
    public Customer deleteCustomer(@PathVariable("id") Long id){
        return customerService.deleteCustomer(id);
    }
}
