package com.fer.coursemanagement.dao;

import com.fer.coursemanagement.domain.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    int countByUsername(String username);
    boolean existsByUsernameAndIdNot(String username, Long id);

    Optional<Employee> findByUsername(String username);

    Optional<Employee> findByEmail(String email);
}

