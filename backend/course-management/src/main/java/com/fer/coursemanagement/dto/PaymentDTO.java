package com.fer.coursemanagement.dto;

import com.fer.coursemanagement.domain.Payment;

public class PaymentDTO {

    private Long id;
    private Long customerId;
    private Long courseId;
    private Long paymentTypeId;
    private double price;
    private int discount;

    public PaymentDTO(){
    }

    public PaymentDTO(Payment payment) {
        this.id = payment.getId();
        this.price = payment.getPrice();
        this.discount = payment.getDiscount();

        if(payment.getCustomer()!=null){
            this.customerId=payment.getCustomer().getId();
        }

        if(payment.getCourse()!=null){
            this.courseId=payment.getCourse().getId();
        }

        if(payment.getPaymentType()!=null){
            this.paymentTypeId=payment.getPaymentType().getId();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public Long getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(Long paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }
}
