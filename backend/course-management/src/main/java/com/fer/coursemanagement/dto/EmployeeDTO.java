package com.fer.coursemanagement.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fer.coursemanagement.domain.Employee;

import java.util.Date;

public class EmployeeDTO {

    private Long id;
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String email;

    @JsonFormat(pattern="yyyy-MM-dd")
    private Date dateOfBirth;
    private Long roleId;

    public EmployeeDTO(){
    }

    public EmployeeDTO(Employee employee){
        this.id=employee.getId();
        this.username=employee.getUsername();
        this.password=employee.getPassword();
        this.firstName=employee.getFirstName();
        this.lastName=employee.getLastName();
        this.email=employee.getEmail();
        this.dateOfBirth=employee.getDateOfBirth();

        if(employee.getRole()!=null){
            this.roleId=employee.getRole().getId();
        }

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }
}
