package com.fer.coursemanagement.rest;

import com.fer.coursemanagement.domain.PaymentType;
import com.fer.coursemanagement.service.PaymentTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/paymentTypes")
public class PaymentTypeController {

    @Autowired
    PaymentTypeService paymentTypeService;

    @GetMapping
    public List<PaymentType> listAll(){
        return paymentTypeService.listAll();
    }

    @GetMapping(path="/{id}")
    public PaymentType getPaymentType(@PathVariable("id") long id){
        return paymentTypeService.get(id);
    }

    @PostMapping
    public PaymentType createPaymentType(@RequestBody PaymentType paymentType){
        return paymentTypeService.createPaymentType(paymentType);
    }

    @PutMapping(path = "/{id}")
    public PaymentType updatePaymentType(@PathVariable("id") Long id, @RequestBody PaymentType paymentType){
        if(!paymentType.getId().equals(id)){
            throw new IllegalArgumentException();
        }
        return paymentTypeService.updatePaymentType(paymentType);
    }

    @DeleteMapping(path = "/{id}")
    public PaymentType deletePaymentType(@PathVariable("id") Long id) {
        return paymentTypeService.deletePaymentType(id);
    }

}
