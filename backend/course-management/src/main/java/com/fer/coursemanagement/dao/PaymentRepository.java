package com.fer.coursemanagement.dao;

import com.fer.coursemanagement.domain.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PaymentRepository extends JpaRepository<Payment,Long> {

    Optional<Payment> findByCustomerId(long customerId);

    Optional<Payment> findByCourseId(long courseId);

    Optional<Payment> findByPaymentTypeId(long paymentTypeId);
}
