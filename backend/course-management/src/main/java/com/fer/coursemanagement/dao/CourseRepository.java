package com.fer.coursemanagement.dao;

import com.fer.coursemanagement.domain.Course;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CourseRepository extends JpaRepository<Course, Long> {

    int countByName(String name);
    boolean existsByNameAndIdNot(String name, Long id);

    Optional<Course> findByName(String name);

    Optional<Course> findByLecturerId(long lecturerId);
}
