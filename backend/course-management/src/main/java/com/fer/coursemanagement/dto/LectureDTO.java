package com.fer.coursemanagement.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fer.coursemanagement.domain.Lecture;

import java.util.Date;

public class LectureDTO {

    private Long id;
    private String title;
    private String description;
    private Long courseId;
    private Integer orderNumber;

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm")
    private Date startTime;

    @JsonFormat(pattern = "dd-MM-yyyy HH:mm")
    private Date endTime;

    public LectureDTO(){
    }

    public LectureDTO(Lecture lecture) {
        this.id = lecture.getId();
        this.title = lecture.getTitle();
        this.description = lecture.getDescription();
        this.orderNumber = lecture.getOrderNumber();
        this.startTime = lecture.getStartTime();
        this.endTime = lecture.getEndTime();

        if(lecture.getCourse()!=null){
            this.courseId=lecture.getCourse().getId();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }

    public Integer getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}
