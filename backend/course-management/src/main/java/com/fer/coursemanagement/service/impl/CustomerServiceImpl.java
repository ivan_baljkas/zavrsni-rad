package com.fer.coursemanagement.service.impl;

import com.fer.coursemanagement.dao.CustomerRepository;
import com.fer.coursemanagement.domain.Customer;
import com.fer.coursemanagement.service.CourseService;
import com.fer.coursemanagement.service.CustomerService;
import com.fer.coursemanagement.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CourseService courseService;

    @Autowired
    private PaymentService paymentService;

    @Override
    public Customer get(long customerId) {
        return customerRepository.findById(customerId)
                .orElseThrow(()->new NullPointerException());
    }

    @Override
    public List<Customer> listAll() {
        return customerRepository.findAll();
    }


    @Override
    public Customer findByUsername(String username) {
        Optional<Customer> customer=customerRepository.findByUsername(username);

        return customer.orElseThrow(()->new NullPointerException());
    }

    @Override
    public Customer createCustomer(Customer customer) {
        validate(customer);
        Assert.isNull(customer.getId(), "Customer ID must be null, not: " + customer.getId());

        if(customerRepository.countByUsername(customer.getUsername())>0){
            throw new IllegalArgumentException();
        }

        return customerRepository.save(customer);

    }

    @Override
    public Customer updateCustomer(Customer customer) {
        validate(customer);
        if(!customerRepository.existsById(customer.getId())){
            throw new IllegalArgumentException();
        }
        if(customerRepository.existsByUsernameAndIdNot(customer.getUsername(),customer.getId())){
            throw new IllegalArgumentException();
        }

        return customerRepository.save(customer);
    }

    @Override
    public Customer deleteCustomer(long customerId) {
        Customer customer= get(customerId);


        courseService.listAll().stream().filter(c->c.getMembersId().contains(customerId))
                .forEach(c->{
                    List<Long> list = c.getMembersId();
                    list.remove(customerId);
                    c.setMembersId(list);
                    courseService.updateCourse(c);
                });

        paymentService.listAll().stream().filter(p->p.getCustomerId().equals(customerId))
                .forEach(p->{
                    p.setCustomerId(null);
                    paymentService.updatePayment(p);
                });

        customerRepository.delete(customer);
        return customer;
    }

    private void validate(Customer customer) {
        Assert.notNull(customer, "Customer object must be given");
        Assert.hasText(customer.getUsername(), "Username must be given");
        Assert.hasText(customer.getEmail(), "Email must be given");
        Assert.hasText(customer.getFirstName(), "First name must be given");
        Assert.hasText(customer.getLastName(), "Last name must be given");
        Assert.notNull(customer.getDateOfBirth(), "Date of birth must be given");
    }
}
