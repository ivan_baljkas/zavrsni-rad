package com.fer.coursemanagement.dao;

import com.fer.coursemanagement.domain.PaymentType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PaymentTypeRepository extends JpaRepository<PaymentType,Long> {

    Optional<PaymentType> findByName(String name);

    int countByName(String name);

    boolean existsByNameAndIdNot(String name, Long id);

}
