package com.fer.coursemanagement.service.impl;

import com.fer.coursemanagement.dao.RoleRepository;
import com.fer.coursemanagement.domain.Role;
import com.fer.coursemanagement.service.EmployeeService;
import com.fer.coursemanagement.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private EmployeeService employeeService;

    @Override
    public List<Role> listAll() {
        return roleRepository.findAll();
    }

    @Override
    public Role get(long roleId) {
        return roleRepository.findById(roleId)
                .orElseThrow(()->new NullPointerException());
    }

    @Override
    public Role createRole(Role role) {
        validate(role);
        Assert.isNull(role.getId(), "Role ID must be null, not: " + role.getId());
        if(roleRepository.countByName(role.getName())>0){
            throw new IllegalArgumentException();
        }

        return roleRepository.save(role);
    }

    @Override
    public Role updateRole(Role role) {
        validate(role);
        long roleId = role.getId();
        if(!roleRepository.existsById(roleId)){
            throw new IllegalArgumentException();
        }
        if(roleRepository.existsByNameAndIdNot(role.getName(),role.getId())){
            throw new IllegalArgumentException();
        }

        return roleRepository.save(role);
    }

    @Override
    public Role deleteRole(long roleId) {
        Role role = get(roleId);

        employeeService.listAll().stream().filter(e->e.getRoleId().equals(roleId))
                .forEach(e->{
                    e.setRoleId(null);
                    employeeService.updateEmployee(e);
                });

        roleRepository.delete(role);
        return role;
    }

    private void validate(Role role) {
        Assert.notNull(role, "Role object must be given");
        Assert.hasText(role.getName(), "Role name must be given");
    }
}
