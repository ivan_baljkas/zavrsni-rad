package com.fer.coursemanagement.service.impl;

import com.fer.coursemanagement.dao.LectureRepository;
import com.fer.coursemanagement.domain.Lecture;
import com.fer.coursemanagement.dto.LectureDTO;
import com.fer.coursemanagement.service.CourseService;
import com.fer.coursemanagement.service.LectureService;
import org.apache.commons.lang3.time.DateUtils;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class LectureServiceImpl implements LectureService {

    @Autowired
    private CourseService courseService;

    @Autowired
    private LectureRepository lectureRepository;

    @Autowired
    private KieSession session;

    public int countLecturesWithSameCourse(LectureDTO dto){
       return (int)lectureRepository.findAll()
                .stream()
                .filter(l->l.getCourse().getId().equals(dto.getCourseId()))
                .count();
    }

    @Override
    public Lecture get(long lectureId) {
      return  lectureRepository.findById(lectureId)
                .orElseThrow(()->new NullPointerException());
    }

    @Override
    public List<LectureDTO> listAll() {
        List<LectureDTO> list = new ArrayList<>();
        lectureRepository.findAll().forEach(l->list.add(new LectureDTO(l)));

        return list;
    }

    @Override
    public LectureDTO findById(long lectureId) {
        Optional<Lecture> lecture=lectureRepository.findById(lectureId);

        if(!lecture.isPresent()){
            throw new NullPointerException();
        }

        return new LectureDTO(lecture.get());
    }

    @Override
    public LectureDTO createLecture(LectureDTO dto) {
        validate(dto);
        Assert.isNull(dto.getId(), "Lecture ID must be null, not: " + dto.getId());

        session.setGlobal("nextOrderNumber",countLecturesWithSameCourse(dto));
        session.insert(dto);
        session.fireAllRules();

        return new LectureDTO(lectureRepository.save(lectureFromDTO(dto)));
    }

    @Override
    public LectureDTO updateLecture(LectureDTO dto) {

        validate(dto);

        if(!lectureRepository.existsById(dto.getId())){
            throw new IllegalArgumentException();
        }

        return new LectureDTO(lectureRepository.save(lectureFromDTO(dto)));
    }

    @Override
    public LectureDTO deleteLecture(long lectureId) {
        LectureDTO dto = findById(lectureId);

        lectureRepository.findAll().stream().filter(l->l.getCourse().getId().equals(dto.getCourseId()))
                .filter(l->l.getOrderNumber()>dto.getOrderNumber())
                .forEach(l->{
                    l.setOrderNumber(l.getOrderNumber()-1);
                    updateLecture(new LectureDTO(l));
                });

        lectureRepository.delete(lectureFromDTO(dto));
        return dto;
    }

    private Lecture lectureFromDTO(LectureDTO dto){
        Lecture lecture = new Lecture();

        lecture.setId(dto.getId());
        lecture.setTitle(dto.getTitle());
        lecture.setDescription((dto.getDescription()));
        lecture.setOrderNumber(dto.getOrderNumber());
        lecture.setStartTime(dto.getStartTime());
        lecture.setEndTime(dto.getEndTime());

        if(dto.getCourseId()!=null){
            lecture.setCourse(courseService.get(dto.getCourseId()));
        }

        return lecture;
    }

    private void validate(LectureDTO dto){
        Assert.notNull(dto, "Lecture object must be given");
        Assert.hasText(dto.getTitle(), "Title must be given");
        Assert.notNull(dto.getStartTime(), "Start time must be given");
    }
}
