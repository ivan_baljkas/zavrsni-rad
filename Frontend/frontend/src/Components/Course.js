import React from "react";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import {CardHeader} from "@material-ui/core";

const Course = (props) => {
    return (
        <div>
            {props.course ? (
                <Card>
                    <CardContent>
                        <Typography gutterBottom variant={"h4"}>
                            {props.course.name}
                        </Typography>
                        <Typography gutterBottom variant={"subtitle1"}>
                            {props.course.description}
                        </Typography>
                        <Typography >
                            {"Price : " + props.course.price+ "€"}
                        </Typography>
                    </CardContent>
                </Card>
            ): null}
        </div>
    )
}
export default Course