import React, {useState, useEffect} from 'react';
import {Link as RouterLink, withRouter} from 'react-router-dom';
import PropTypes from 'prop-types';
import validate from 'validate.js';
import {makeStyles} from '@material-ui/core/styles';
import Delete from '@material-ui/icons/Delete';
import Save from '@material-ui/icons/Save';
import Create from '@material-ui/icons/Create';
import Messages from './Messages/Messages';
import {
    Grid,
    Button,
    Input,
    InputLabel,
    IconButton,
    Select,
    TextField,
    Link,
    FormHelperText,
    Checkbox,
    MenuList,
    MenuItem,
    Typography
} from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import axios from 'axios';

const schema = {
    serviceId: {
        presence: {allowEmpty: true, message: 'is required'},
        length: {
            maximum: 32
        }
    },
    name: {
        presence: {allowEmpty: false, message: 'is required'},
        length: {
            maximum: 32
        }
    },
    surname: {
        presence: {allowEmpty: false, message: 'is required'},
        length: {
            maximum: 32
        }
    },
    group: {
        presence: {allowEmpty: false, message: 'is required'},
        length: {
            maximum: 32
        }
    },
    serviceDescription: {
        presence: {allowEmpty: false, message: 'is required'},
        length: {
            maximum: 256
        }
    },
    leader: {
        presence: {allowEmpty: false, message: 'is required'},
        length: {
            maximum: 32
        }
    },
    pricePerHour: {
        presence: {allowEmpty: false, message: 'is required'},
    }
};

const useStyles = makeStyles(theme => ({
    root: {
        backgroundColor: theme.palette.background.default,
        height: '100%'
    },
    grid: {
        height: '100%'
    },
    name: {
        marginTop: theme.spacing(3),
        color: theme.palette.white
    },
    bio: {
        color: theme.palette.white
    },
    contentContainer: {},
    content: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column'
    },
    contentHeader: {
        display: 'flex',
        alignItems: 'center',
        paddingTop: theme.spacing(5),
        paddingBottom: theme.spacing(2),
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2)
    },
    logoImage: {
        marginLeft: theme.spacing(4)
    },
    contentBody: {
        flexGrow: 1,
        display: 'flex',
        alignItems: 'center',
    },
    form: {
        paddingLeft: 100,
        paddingRight: 100,
        paddingBottom: 125,
        flexBasis: 700,
    },
    title: {
        marginTop: theme.spacing(3)
    },
    textField: {
        marginTop: theme.spacing(2)
    },
    inputLabel: {
        marginTop: theme.spacing(4)
    },
    policy: {
        marginTop: theme.spacing(1),
        display: 'flex',
        alignItems: 'center'
    },
    policyCheckbox: {
        marginLeft: '-14px'
    },
    Button: {
        margin: theme.spacing(2, 0),
        width: 100
    },
    ButtonDelete: {
        margin: theme.spacing(2, 0),
        width: 100,
        marginRight: 30,
        backgroundColor: 'red'
    }
}));

const LectureManagement = props => {
    const {history} = props;

    const helpSID = window.localStorage.getItem("lectureId");
    let SID = helpSID == "null" ? null : helpSID;

    const helpSID2 = window.localStorage.getItem("courseId");
    let SID2 = helpSID2 == "null" ? null : helpSID2;

    const classes = useStyles();

    const [formState, setFormState] = useState({
        isValid: false,
        isSaveValid: false,
        isDeleteValid: false,
        uvjetMessage: false,
        vrijednosti: [],
        values: {
            title: '',
            description: '',
            orderNumber: '',
            course: '',
            startTime: '',
            endTime: ''
        },
        touched: {},
        errors: {},
        lectures: []
    });

    let isNull = true;

    useEffect(() => {

        
        axios.get("https://tecajevi-i-lekcije.herokuapp.com/lectures")
            .then(res => {

                const helpLectures = res.data;
                if (SID != null) {
                    isNull = false;
                    helpLectures.forEach(lecture => {
                        if (lecture.id == SID) {
                            axios.get("https://tecajevi-i-lekcije.herokuapp.com/courses")
                                .then(response => {
                                    const newCourses = response.data;
                                    newCourses.forEach(course => {
                                        if (course.id == SID2) {
                                            setFormState(formState => ({
                                                ...formState,
                                                values: {
                                                    ...formState.values,
                                                    course: course.name
                                                }
                                            }));
                                        }
                                    });
                                });
                                const startTimeString =lecture.startTime;
                                const arrayStartTime = startTimeString.split(" ");
                                let date = arrayStartTime[0];
                                let time = arrayStartTime[1];
                                let arrayDate = date.split("-");
                                let arrayTime = time.split(":");
                                const formattedStartTime = arrayDate[2]+"-"+arrayDate[1]+"-"+arrayDate[0]+"T"+arrayTime[0]+":"+arrayTime[1];

                                const endTimeString =lecture.endTime;
                                const arrayEndTime = endTimeString.split(" ");
                                date = arrayEndTime [0];
                                time = arrayEndTime [1];
                                arrayDate = date.split("-");
                                arrayTime = time.split(":");
                                const formattedEndTime = arrayDate[2]+"-"+arrayDate[1]+"-"+arrayDate[0]+"T"+arrayTime[0]+":"+arrayTime[1];

                            setFormState(formState => ({
                                ...formState,
                                values: {
                                    ...formState.values,
                                    title: lecture.title,
                                    description: lecture.description,
                                    startTime: formattedStartTime,
                                    endTime: formattedEndTime,
                                    orderNumber:lecture.orderNumber
                                }
                            }));
                        }
                    });
                }

                setFormState(formState => ({
                    ...formState,
                    vrijednosti: res.data,
                    lectures: res.data,
                }))
            });


        if (SID != null) {
            setFormState(formState => ({
                ...formState,
                isSaveValid: true,
                isDeleteValid: true
            }));
        }
    }, []);


    const handleChange = event => {
        event.persist();
        
        setFormState(formState => ({
            ...formState,
            values: {
                ...formState.values,
                [event.target.name]:
                    event.target.type === 'checkbox'
                        ? event.target.checked
                        : event.target.value
            },
            touched: {
                ...formState.touched,
                [event.target.name]: true
            }
        }));


        if (document.getElementById('title').value === ''
            || document.getElementById('description').value === ''
            || document.getElementById('startTime').value === '') {
            setFormState(formState => ({
                ...formState,
                isSaveValid: false
            }));
        } else {
            setFormState(formState => ({
                ...formState,
                isSaveValid: true
            }));
        }


    };



    const handleBack = () => {
        history.goBack();
    };


    const handleDelete = event => {
        event.preventDefault();

        setFormState(formState => ({
            ...formState,
            uvjetMessage: true
        }));

        if (SID != null) {
            axios.delete(`https://tecajevi-i-lekcije.herokuapp.com/lectures/${SID}`)
                .then(res => {
                    console.log(res);
                    console.log(res.data);
                }).catch(e => {
                console.log(e);
            });
        }

    }


    const handleSave = event => {
        event.preventDefault();

        setFormState(formState => ({
            ...formState,
            uvjetMessage: true
        }));

        const timeStartString = document.getElementById('startTime').value.toString();
        const arrayStartTime = timeStartString.split("T");
        const dateStartArray = arrayStartTime[0].split("-");
        const formatedStartTime = dateStartArray[2] + '-' + dateStartArray[1] + '-' + dateStartArray[0] + ' ' + arrayStartTime[1];

        const timeEndString = document.getElementById('endTime').value.toString();
        const arrayEndTime = timeEndString.split("T");
        const dateEndArray = arrayEndTime[0].split("-");
        const formatedEndTime = dateEndArray[2] + '-' + dateEndArray[1] + '-' + dateEndArray[0] + ' ' + arrayEndTime[1];

        if (SID == null) {

            const param = {
                title: document.getElementById('title').value,
                description: document.getElementById('description').value,
                courseId: SID2,
                startTime: formatedStartTime
            };
            console.log(param);
            axios.post('https://tecajevi-i-lekcije.herokuapp.com/lectures', param)
                .then(res => {
                    console.log(res);
                }).catch(e => {
                console.log(e);
            });
           
            return;
        }

        if (SID != null) {
            const param2 = {
                id: SID,
                title: document.getElementById('title').value,
                description: document.getElementById('description').value,
                courseId: SID2,
                orderNumber:document.getElementById('orderNumber').value,
                startTime: formatedStartTime,
                endTime : formatedEndTime
            };
            console.log(param2);
            axios.put(`https://tecajevi-i-lekcije.herokuapp.com/lectures/${SID}`, param2)
                .then(res => {
                    console.log(res);
                    console.log(res.data);
                }).catch(e => {
                console.log(e);
            });
            
           
        }
        
    };

    const hasError = field =>
        formState.touched[field] && formState.errors[field] ? true : false;

    return (
        <div className={classes.root}>
            <Grid
                className={classes.grid}
                container
            >
                <Grid
                    className={classes.content}
                    item
                    lg={7}
                    xs={12}
                >
                    <div className={classes.content}>
                        <div className={classes.contentHeader}>
                            <IconButton onClick={handleBack}>
                                <ArrowBackIcon/>
                            </IconButton>
                        </div>
                        <div className={classes.contentBody}>
                            <form
                                className={classes.form}
                            >
                                <Typography
                                    className={classes.title}
                                    variant="h2"
                                >
                                    Lecture Management
                                </Typography>
                                <Typography
                                    color="textSecondary"
                                    gutterBottom
                                >
                                </Typography>
                                <TextField
                                    className={classes.textField}
                                    id="title"
                                    error={hasError('title')}
                                    fullWidth
                                    helperText={
                                        hasError('title') ? formState.errors.title[0] : null
                                    }
                                    label="Title"
                                    name="title"
                                    onChange={handleChange}
                                    type="text"
                                    value={formState.values.title || ''}
                                    variant="outlined"
                                />
                                <TextField
                                    className={classes.textField}
                                    id="description"
                                    error={hasError('description')}
                                    fullWidth
                                    helperText={
                                        hasError('description') ? formState.errors.description[0] : null
                                    }
                                    label="Description"
                                    name="description"
                                    onChange={handleChange}
                                    type="text"
                                    value={formState.values.description || ''}
                                    variant="outlined"
                                />
                                <TextField
                                    className={classes.textField}
                                    error={hasError('course')}
                                    disabled="true"
                                    id="course"
                                    fullWidth
                                    helperText={
                                        hasError('course') ? formState.errors.course[0] : null
                                    }
                                    label="Course"
                                    name="course"
                                    onChange={handleChange}
                                    type="text"
                                    InputProps={{ inputProps: { min: 0 } }}
                                    value={formState.values.course || ''}
                                    variant="outlined"
                                />
                                <TextField
                                    className={classes.textField}
                                    error={hasError('orderNumber')}
                                    id="orderNumber"
                                    disabled="true"
                                    fullWidth
                                    helperText={
                                        hasError('orderNumber') ? formState.errors.orderNumber[0] : null
                                    }
                                    label="Order Number"
                                    name="orderNumber"
                                    onChange={handleChange}
                                    type="number"
                                    InputProps={{ inputProps: { min: 0 } }}
                                    value={formState.values.orderNumber || ''}
                                    variant="outlined"
                                />
                                <InputLabel
                                    id="demo-simple-select-label"
                                    className={classes.inputLabel}
                                >
                                    Start Time
                                </InputLabel>
                                <TextField
                                    className={classes.textField}
                                    error={hasError('startTime')}
                                    id="startTime"
                                    fullWidth
                                    helperText={
                                        hasError('startTime') ? formState.errors.startTime[0] : null
                                    }
                                    name="startTime"
                                    onChange={handleChange}
                                    type="datetime-local"
                                    InputProps={{ inputProps: { min: 0 } }}
                                    value={formState.values.startTime || ''}
                                    variant="outlined"
                                />
                                <InputLabel
                                    id="demo-simple-select-label"
                                    className={classes.inputLabel}
                                >
                                    End Time
                                </InputLabel>
                                <TextField
                                    className={classes.textField}
                                    error={hasError('endTime')}
                                    disabled="true"
                                    id="endTime"
                                    fullWidth
                                    helperText={
                                        hasError('endTime') ? formState.errors.endTime[0] : null
                                    }
                                    name="endTime"
                                    onChange={handleChange}
                                    type="datetime-local"
                                    InputProps={{ inputProps: { min: 0 } }}
                                    value={formState.values.endTime || ''}
                                    variant="outlined"
                                />
                                <Button
                                    className={classes.ButtonDelete}
                                    id="deleteBtn"
                                    color="primary"
                                    disabled={!formState.isDeleteValid}
                                    size="large"
                                    type="submit"
                                    variant="contained"
                                    onClick={handleDelete}
                                >
                                    <Delete/>
                                    Delete
                                </Button>
                                <Button
                                    className={classes.Button}
                                    id="saveBtn"
                                    color="primary"
                                    disabled={!formState.isSaveValid}
                                    size="large"
                                    type="submit"
                                    variant="contained"
                                    onClick={handleSave}
                                >
                                    <Save/>
                                    Save
                                </Button>
                                <Messages uvjet={formState.uvjetMessage}/>
                            </form>
                        </div>
                    </div>
                </Grid>
            </Grid>
        </div>
    );
};

LectureManagement.propTypes = {
    history: PropTypes.object
};

export default LectureManagement;