import React, {useState, useEffect} from 'react';
import {Link as RouterLink, withRouter} from 'react-router-dom';
import PropTypes from 'prop-types';
import validate from 'validate.js';
import {makeStyles} from '@material-ui/core/styles';
import Delete from '@material-ui/icons/Delete';
import Save from '@material-ui/icons/Save';
import Create from '@material-ui/icons/Create';
import Messages from './Messages/Messages';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import {
    Grid,
    Button,
    Input,
    InputLabel,
    IconButton,
    Select,
    TextField,
    Link,
    FormHelperText,
    Checkbox,
    MenuList,
    MenuItem,
    Typography
} from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import axios from 'axios';

const schema = {
    serviceId: {
        presence: {allowEmpty: true, message: 'is required'},
        length: {
            maximum: 32
        }
    },
    name: {
        presence: {allowEmpty: false, message: 'is required'},
        length: {
            maximum: 32
        }
    },
    surname: {
        presence: {allowEmpty: false, message: 'is required'},
        length: {
            maximum: 32
        }
    },
    group: {
        presence: {allowEmpty: false, message: 'is required'},
        length: {
            maximum: 32
        }
    },
    serviceDescription: {
        presence: {allowEmpty: false, message: 'is required'},
        length: {
            maximum: 256
        }
    },
    leader: {
        presence: {allowEmpty: false, message: 'is required'},
        length: {
            maximum: 32
        }
    },
    pricePerHour: {
        presence: {allowEmpty: false, message: 'is required'},
    }
};

const useStyles = makeStyles(theme => ({
    root: {
        backgroundColor: theme.palette.background.default,
        height: '100%'
    },
    grid: {
        height: '100%'
    },
    name: {
        marginTop: theme.spacing(3),
        color: theme.palette.white
    },
    bio: {
        color: theme.palette.white
    },
    contentContainer: {},
    content: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column'
    },
    contentHeader: {
        display: 'flex',
        alignItems: 'center',
        paddingTop: theme.spacing(5),
        paddingBottom: theme.spacing(2),
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2)
    },
    logoImage: {
        marginLeft: theme.spacing(4)
    },
    contentBody: {
        flexGrow: 1,
        display: 'flex',
        alignItems: 'center',
    },
    form: {
        paddingLeft: 100,
        paddingRight: 100,
        paddingBottom: 125,
        flexBasis: 700,
    },
    title: {
        marginTop: theme.spacing(3)
    },
    textField: {
        marginTop: theme.spacing(2)
    },
    inputLabel: {
        marginTop: theme.spacing(4)
    },
    policy: {
        marginTop: theme.spacing(1),
        display: 'flex',
        alignItems: 'center'
    },
    policyCheckbox: {
        marginLeft: '-14px'
    },
    Button: {
        margin: theme.spacing(2, 0),
        width: 100
    },
    ButtonDelete: {
        margin: theme.spacing(2, 0),
        width: 100,
        marginRight: 30,
        backgroundColor: 'red'
    }
}));

const CourseManagement = props => {
    const {history} = props;

    const helpSID = window.localStorage.getItem("courseId");
    let SID = helpSID == "null" ? null : helpSID;

    const classes = useStyles();

    const [formState, setFormState] = useState({
        isValid: false,
        isSaveValid: false,
        isDeleteValid: false,
        isBusinessSelected: false,
        isLecturerSelected: false,
        uvjetMessage: false,
        vrijednosti: [],
        values: {
            name: '',
            description: '',
            capacity: 0,
            lecturer: '',
            price: 0,
            freeSeats: '',
            membersId:[]
        },
        touched: {},
        errors: {},
        lecturers: [],
        courses: [],
        businesses: [],
        customers:[],
        customers2:[],
        selectedLecturer: {},
        selectedBusiness: {}
    });

    let isNull = true;

    useEffect(() => {

        
        axios.get("https://tecajevi-i-lekcije.herokuapp.com/courses")
            .then(res => {

                const helpCourses = res.data;
                if (SID != null) {
                    isNull = false;
                    helpCourses.forEach(course => {
                        if (course.id == SID) {
                           
                            axios.get("https://tecajevi-i-lekcije.herokuapp.com/employees")
                                .then(response => {
                                    const newLecturers = response.data;
                                    newLecturers.forEach(lec => {
                                        if (lec.id === course.lecturerId) {
                                            setFormState(formState => ({
                                                ...formState,
                                                values: {
                                                    ...formState.values,
                                                    lecturer: lec.username
                                                },
                                                selectedLecturer: lec.username
                                            }));
                                        }
                                    });
                                });
                            setFormState(formState => ({
                                ...formState,
                                values: {
                                    ...formState.values,
                                    name: course.name,
                                    description: course.description,
                                    price: course.price,
                                    freeSeats: course.freeSeats,
                                    capacity:course.capacity,
                                    membersId:course.membersId
                                }
                            }));
                        }
                    });
                }

                setFormState(formState => ({
                    ...formState,
                    vrijednosti: res.data,
                    courses: res.data,
                }))
            });

        axios.get("https://tecajevi-i-lekcije.herokuapp.com/employees")
            .then(res => {

                setFormState(formState => ({
                    ...formState,
                    lecturers: res.data
                }))
            });


        if (SID != null) {
            setFormState(formState => ({
                ...formState,
                isSaveValid: true,
                isDeleteValid: true,
                isLecturerSelected: true
            }));
        }


        //---------------------------------------------
        axios.get("https://tecajevi-i-lekcije.herokuapp.com/courses")
            .then(res => {

                const helpCourses = res.data;
                    helpCourses.forEach(course => {
                        if (course.id == SID) {
                           
                            axios.get("https://tecajevi-i-lekcije.herokuapp.com/customers")
                                .then(response => {
                                    const newCustomers = response.data;
                                    newCustomers.forEach(customer => {
                                        if (course.membersId.indexOf(customer.id) === -1) {
                                            setFormState(formState =>({
                                                ...formState,
                                                customers: [...formState.customers,
                                                    { 
                                                        id: customer.id,
                                                        firstName: customer.firstName,
                                                        lastName: customer.lastName,
                                                        username: customer.username,
                                                        email: customer.email}]
                                            }));
                                        }
                                        else{
                                            setFormState(formState =>({
                                                ...formState,
                                                customers2: [...formState.customers2,
                                                    { 
                                                        id: customer.id,
                                                        firstName: customer.firstName,
                                                        lastName: customer.lastName,
                                                        username: customer.username,
                                                        email: customer.email}]
                                            }));
                                        }
                                        
                                    });
                                });
                           
                        }
                    });
                }
            );
    }, []);


    const handleChange = event => {
        event.persist();

        setFormState(formState => ({
            ...formState,
            values: {
                ...formState.values,
                [event.target.name]:
                    event.target.type === 'checkbox'
                        ? event.target.checked
                        : event.target.value
            },
            touched: {
                ...formState.touched,
                [event.target.name]: true
            }
        }));


        if (document.getElementById('name').value === ''
            || document.getElementById('description').value === ''
            || formState.isLecturerSelected === false
            || document.getElementById('price').value === ''
            || document.getElementById('capacity').value === '') {
            setFormState(formState => ({
                ...formState,
                isSaveValid: false
            }));
        } else {
            setFormState(formState => ({
                ...formState,
                isSaveValid: true
            }));
        }


    };


    const handleLecturer = event => {
        setFormState(formState => ({
            ...formState,
            isLecturerSelected: true,
            selectedLecturer: event.target.value,
            values: {
                ...formState.values,
                [event.target.name]:
                    event.target.type === 'checkbox'
                        ? event.target.checked
                        : event.target.value
            },
            touched: {
                ...formState.touched,
                [event.target.name]: true
            }
        }));

        if (document.getElementById('name').value === ''
            || document.getElementById('description').value === ''
            || document.getElementById('price').value === ''
            || document.getElementById('capacity').value === '') {
            setFormState(formState => ({
                ...formState,
                isSaveValid: false
            }));
        } else {
            setFormState(formState => ({
                ...formState,
                isSaveValid: true
            }));
        }
    };

    const handleBack = () => {
        history.goBack();
    };


    const handleDelete = event => {
        event.preventDefault();

        setFormState(formState => ({
            ...formState,
            uvjetMessage: true
        }));

        if (SID != null) {
            axios.delete(`https://tecajevi-i-lekcije.herokuapp.com/courses/${SID}`)
                .then(res => {
                    console.log(res);
                    console.log(res.data);
                }).catch(e => {
                console.log(e);
            });
        }

    }


    const handleSave = event => {
        event.preventDefault();

        setFormState(formState => ({
            ...formState,
            uvjetMessage: true
        }));

        let helpLecturerId = 99;

        formState.lecturers.forEach(current => {
            if (current.username === formState.selectedLecturer) {
                helpLecturerId = current.id;
            }
        });

        if (SID == null) {

            const param = {
                name: document.getElementById('name').value,
                description: document.getElementById('description').value,
                lecturerId: helpLecturerId,
                price: document.getElementById('price').value,
                capacity: document.getElementById('capacity').value,
                membersId:[]
            };
            console.log(param);
            axios.post('https://tecajevi-i-lekcije.herokuapp.com/courses', param)
                .then(res => {
                    console.log(res);
                }).catch(e => {
                console.log(e);
            });
           
            return;
        }

        if (SID != null) {
            const param2 = {
                id: SID,
                name: document.getElementById('name').value,
                description: document.getElementById('description').value,
                lecturerId: helpLecturerId,
                price: document.getElementById('price').value,
                capacity: document.getElementById('capacity').value,
                membersId:formState.values.membersId
            };
            console.log(param2);
            axios.put(`https://tecajevi-i-lekcije.herokuapp.com/courses/${SID}`, param2)
                .then(res => {
                    console.log(res);
                    console.log(res.data);
                }).catch(e => {
                console.log(e);
            });
            
           
        }
        
    };

    const handleAdd = (rowId) =>{
        
        //trebalo bi ga linkati na ekran upravljanje djelatnostima sa propsom id koji ce biti jednak rowId argumentu funkcije

        axios.get(`https://tecajevi-i-lekcije.herokuapp.com/customers/${rowId}`)
                .then(response => {
                    setFormState(formState => ({
                        ...formState,
                        customers:formState.customers.filter(customer=>customer.id!==rowId),
                        customers2:[...formState.customers2,response.data],
                        values: {
                            ...formState.values,
                            membersId:[...formState.values.membersId, rowId]
                        }
                    }));       
            });

        

    }

    const handleDeleteMember = (rowId) =>{
        
        //trebalo bi ga linkati na ekran upravljanje djelatnostima sa propsom id koji ce biti jednak rowId argumentu funkcije

        let newMembers = [];

        for(var i=0; i<formState.values.membersId.length; i++){
            if(formState.values.membersId[i] !== rowId)
                newMembers.push(formState.values.membersId[i]);
        }

        axios.get(`https://tecajevi-i-lekcije.herokuapp.com/customers/${rowId}`)
                .then(response => {
                    setFormState(formState => ({
                        ...formState,
                        customers2:formState.customers2.filter(customer=>customer.id!==rowId),
                        customers:[...formState.customers,response.data],
                        values: {
                            ...formState.values,
                            membersId:newMembers
                        }
                    }));       
            });

    }

    const hasError = field =>
        formState.touched[field] && formState.errors[field] ? true : false;

    return (
        <div className={classes.root}>
            <Grid
                className={classes.grid}
                container
            >
                <Grid
                    className={classes.content}
                    item
                    lg={7}
                    xs={12}
                >
                    <div className={classes.content}>
                        <div className={classes.contentHeader}>
                            <IconButton onClick={handleBack}>
                                <ArrowBackIcon/>
                            </IconButton>
                        </div>
                        <div className={classes.contentBody}>
                            <form
                                className={classes.form}
                            >
                                <Typography
                                    className={classes.title}
                                    variant="h2"
                                >
                                    Course Management
                                </Typography>
                                <Typography
                                    color="textSecondary"
                                    gutterBottom
                                >
                                </Typography>
                                <TextField
                                    className={classes.textField}
                                    id="name"
                                    error={hasError('name')}
                                    fullWidth
                                    helperText={
                                        hasError('name') ? formState.errors.name[0] : null
                                    }
                                    label="Name"
                                    name="name"
                                    onChange={handleChange}
                                    type="text"
                                    value={formState.values.name || ''}
                                    variant="outlined"
                                />
                                <TextField
                                    className={classes.textField}
                                    id="description"
                                    error={hasError('description')}
                                    fullWidth
                                    helperText={
                                        hasError('description') ? formState.errors.description[0] : null
                                    }
                                    label="Description"
                                    name="description"
                                    onChange={handleChange}
                                    type="text"
                                    value={formState.values.description || ''}
                                    variant="outlined"
                                />
                                <InputLabel
                                    id="demo-simple-select-label"
                                    className={classes.inputLabel}
                                >
                                    Select a lecturer:
                                </InputLabel>
                                <Select
                                    //className={classes.textField}
                                    labelId="demo-simple-select-label"
                                    id="lecturer"
                                    error={hasError('lecturer')}
                                    fullWidth
                                    helperText={
                                        hasError('lecturer') ? formState.errors.lecturer[0] : null
                                    }
                                    name="lecturer"
                                    onChange={handleLecturer}
                                    type="text"
                                    value={formState.values.lecturer}
                                    variant="outlined"
                                >
                                    {formState.lecturers.map(current => (
                                        <MenuItem
                                            value={current.username}
                                            id={current.id}
                                            onChange={handleChange}>
                                            {current.username}
                                        </MenuItem>
                                    ))};
                                </Select>
                                <TextField
                                    className={classes.textField}
                                    error={hasError('price')}
                                    id="price"
                                    fullWidth
                                    helperText={
                                        hasError('price') ? formState.errors.price[0] : null
                                    }
                                    label="Price"
                                    name="price"
                                    onChange={handleChange}
                                    type="number"
                                    InputProps={{ inputProps: { min: 0 } }}
                                    value={formState.values.price || ''}
                                    variant="outlined"
                                />
                                <TextField
                                    className={classes.textField}
                                    error={hasError('capacity')}
                                    id="capacity"
                                    fullWidth
                                    helperText={
                                        hasError('capacity') ? formState.errors.capacity[0] : null
                                    }
                                    label="Capacity"
                                    name="capacity"
                                    onChange={handleChange}
                                    type="number"
                                    InputProps={{ inputProps: { min: 0 } }}
                                    value={formState.values.capacity || ''}
                                    variant="outlined"
                                />
                                <TextField
                                    className={classes.textField}
                                    error={hasError('freeSeats')}
                                    disabled="true"
                                    id="freeSeats"
                                    fullWidth
                                    helperText={
                                        hasError('freeSeats') ? formState.errors.freeSeats[0] : null
                                    }
                                    label="Free Seats"
                                    name="freeSeats"
                                    onChange={handleChange}
                                    type="number"
                                    InputProps={{ inputProps: { min: 0 } }}
                                    value={formState.values.freeSeats || ''}
                                    variant="outlined"
                                />
                                <Button
                                    className={classes.ButtonDelete}
                                    id="deleteBtn"
                                    color="primary"
                                    disabled={!formState.isDeleteValid}
                                    size="large"
                                    type="submit"
                                    variant="contained"
                                    onClick={handleDelete}
                                >
                                    <Delete/>
                                    Delete
                                </Button>
                                <Button
                                    className={classes.Button}
                                    id="saveBtn"
                                    color="primary"
                                    disabled={!formState.isSaveValid}
                                    size="large"
                                    type="submit"
                                    variant="contained"
                                    onClick={handleSave}
                                >
                                    <Save/>
                                    Save
                                </Button>
                                <Messages uvjet={formState.uvjetMessage}/>
                                <InputLabel
                                    id="demo-simple-select-label"
                                    className={classes.inputLabel}
                                >
                                    Current Members :
                                </InputLabel>
                            <Paper>
                                <Table>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Id</TableCell>
                                            <TableCell align="left">First Name</TableCell>
                                            <TableCell align="left">Last Name</TableCell>
                                            <TableCell align="left">Username</TableCell>
                                            <TableCell align="left">Email</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {formState.customers2.map(row => (
                                            <TableRow key={row.id}>
                                                <TableCell component="th" scope="row">
                                                    {row.id}
                                                </TableCell>
                                                <TableCell align="left">{row.firstName}</TableCell>
                                                <TableCell align="left">{row.lastName}</TableCell>
                                                <TableCell align="left">{row.username}</TableCell>
                                                <TableCell align="left">{row.email}</TableCell>
                                                <TableCell align="right" onClick={() => handleDeleteMember(row.id)}>
                                                <svg class="svg-icon" viewBox="0 0 20 20">
			                                    <path d="M10.185,1.417c-4.741,0-8.583,3.842-8.583,8.583c0,4.74,3.842,8.582,8.583,8.582S18.768,14.74,18.768,10C18.768,5.259,14.926,1.417,10.185,1.417 M10.185,17.68c-4.235,0-7.679-3.445-7.679-7.68c0-4.235,3.444-7.679,7.679-7.679S17.864,5.765,17.864,10C17.864,14.234,14.42,17.68,10.185,17.68 M10.824,10l2.842-2.844c0.178-0.176,0.178-0.46,0-0.637c-0.177-0.178-0.461-0.178-0.637,0l-2.844,2.841L7.341,6.52c-0.176-0.178-0.46-0.178-0.637,0c-0.178,0.176-0.178,0.461,0,0.637L9.546,10l-2.841,2.844c-0.178,0.176-0.178,0.461,0,0.637c0.178,0.178,0.459,0.178,0.637,0l2.844-2.841l2.844,2.841c0.178,0.178,0.459,0.178,0.637,0c0.178-0.176,0.178-0.461,0-0.637L10.824,10z"></path>
                                                </svg>
                                                </TableCell>

                                            </TableRow>
                                        ))}
                                    </TableBody>
                                </Table>
                            </Paper>
                                <InputLabel
                                    id="demo-simple-select-label"
                                    className={classes.inputLabel}
                                >
                                    Add Member :
                                </InputLabel>
                                <Paper>
                                <Table>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Id</TableCell>
                                            <TableCell align="left">First Name</TableCell>
                                            <TableCell align="left">Last Name</TableCell>
                                            <TableCell align="left">Username</TableCell>
                                            <TableCell align="left">Email</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {formState.customers.map(row => (
                                            <TableRow key={row.id}>
                                                <TableCell component="th" scope="row">
                                                    {row.id}
                                                </TableCell>
                                                <TableCell align="left">{row.firstName}</TableCell>
                                                <TableCell align="left">{row.lastName}</TableCell>
                                                <TableCell align="left">{row.username}</TableCell>
                                                <TableCell align="left">{row.email}</TableCell>
                                                <TableCell align="right" onClick={() => handleAdd(row.id)}>
                                                        <Button
                                                            className={classes.Button}
                                                            id="detailsBtn"
                                                            color="primary"
                                                            size="large"
                                                            type="submit"
                                                            variant="contained"
                                                        >
                                                            Add
                                                        </Button>
                                                    </TableCell>

                                            </TableRow>
                                        ))}
                                    </TableBody>
                                </Table>
                            </Paper>
                            </form>
                        </div>
                    </div>
                </Grid>
            </Grid>
        </div>
    );
};

CourseManagement.propTypes = {
    history: PropTypes.object
};

export default CourseManagement;