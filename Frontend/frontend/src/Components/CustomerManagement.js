import React, {useState, useEffect} from 'react';
import {Link as RouterLink, withRouter} from 'react-router-dom';
import PropTypes from 'prop-types';
import validate from 'validate.js';
import {makeStyles} from '@material-ui/core/styles';
import Delete from '@material-ui/icons/Delete';
import Save from '@material-ui/icons/Save';
import Create from '@material-ui/icons/Create';
import Messages from './Messages/Messages';
import {
    Grid,
    Button,
    Input,
    InputLabel,
    IconButton,
    Select,
    TextField,
    Link,
    FormHelperText,
    Checkbox,
    MenuList,
    MenuItem,
    Typography
} from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import axios from 'axios';

const schema = {
    serviceId: {
        presence: {allowEmpty: true, message: 'is required'},
        length: {
            maximum: 32
        }
    },
    name: {
        presence: {allowEmpty: false, message: 'is required'},
        length: {
            maximum: 32
        }
    },
    surname: {
        presence: {allowEmpty: false, message: 'is required'},
        length: {
            maximum: 32
        }
    },
    group: {
        presence: {allowEmpty: false, message: 'is required'},
        length: {
            maximum: 32
        }
    },
    serviceDescription: {
        presence: {allowEmpty: false, message: 'is required'},
        length: {
            maximum: 256
        }
    },
    leader: {
        presence: {allowEmpty: false, message: 'is required'},
        length: {
            maximum: 32
        }
    },
    pricePerHour: {
        presence: {allowEmpty: false, message: 'is required'},
    }
};

const useStyles = makeStyles(theme => ({
    root: {
        backgroundColor: theme.palette.background.default,
        height: '100%'
    },
    grid: {
        height: '100%'
    },
    name: {
        marginTop: theme.spacing(3),
        color: theme.palette.white
    },
    bio: {
        color: theme.palette.white
    },
    contentContainer: {},
    content: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column'
    },
    contentHeader: {
        display: 'flex',
        alignItems: 'center',
        paddingTop: theme.spacing(5),
        paddingBottom: theme.spacing(2),
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2)
    },
    logoImage: {
        marginLeft: theme.spacing(4)
    },
    contentBody: {
        flexGrow: 1,
        display: 'flex',
        alignItems: 'center',
    },
    form: {
        paddingLeft: 100,
        paddingRight: 100,
        paddingBottom: 125,
        flexBasis: 700,
    },
    title: {
        marginTop: theme.spacing(3)
    },
    textField: {
        marginTop: theme.spacing(2)
    },
    inputLabel: {
        marginTop: theme.spacing(4)
    },
    policy: {
        marginTop: theme.spacing(1),
        display: 'flex',
        alignItems: 'center'
    },
    policyCheckbox: {
        marginLeft: '-14px'
    },
    Button: {
        margin: theme.spacing(2, 0),
        width: 100
    },
    ButtonDelete: {
        margin: theme.spacing(2, 0),
        width: 100,
        marginRight: 30,
        backgroundColor: 'red'
    }
}));

const CustomerManagement = props => {
    const {history} = props;


    const helpSID = window.localStorage.getItem("customerId");
    let SID = helpSID == "null" ? null : helpSID;

    const classes = useStyles();

    const [formState, setFormState] = useState({
        isValid: false,
        isSaveValid: false,
        isDeleteValid: false,
        isRoleSelected: false,
        uvjetMessage: false,
        vrijednosti: [],
        values: {
            username: '',
            password: '',
            firstName: '',
            lastname: '',
            email: '',
            dateOfBirth: '',
            cardNumber: ''
        },
        touched: {},
        errors: {},
        customers: []
    });

    let isNull = true;

    useEffect(() => {
        axios.get("https://tecajevi-i-lekcije.herokuapp.com/customers")
            .then(res => {
                const helpCustomers = res.data;
                if (SID != null) {
                    isNull = false;
                    helpCustomers.forEach(customer => {
                        if (customer.id == SID) {

                            const dateString = customer.dateOfBirth;
                            const arrayDate = dateString.split("-");
                            const newFormatDate = arrayDate[2] + "-" + arrayDate[1] + "-" + arrayDate[0];
                            console.log(newFormatDate);
                            setFormState(formState => ({
                                ...formState,
                                values: {
                                    ...formState.values,
                                    username: customer.username,
                                    password: customer.password,
                                    firstName: customer.firstName,
                                    lastName:customer.lastName,
                                    email: customer.email,
                                    dateOfBirth: customer.dateOfBirth,
                                    cardNumber:customer.cardNumber
                                }
                            }));

                        }
                    });
                }

                setFormState(formState => ({
                    ...formState,
                    vrijednosti: res.data,
                    customers: res.data,
                }))
            });



        if (SID != null) {
            setFormState(formState => ({
                ...formState,
                isSaveValid: true,
                isDeleteValid: true
            }));
        }

    }, []);

    const handleChange = event => {
        event.persist();
        
        setFormState(formState => ({
            ...formState,
            values: {
                ...formState.values,
                [event.target.name]:
                    event.target.type === 'checkbox'
                        ? event.target.checked
                        : event.target.value
            },
            touched: {
                ...formState.touched,
                [event.target.name]: true
            }
        }));

        if (document.getElementById('username').value === ''
            || document.getElementById('password').value === ''
            || document.getElementById('firstName').value === ''
            || document.getElementById('lastName').value === ''
            || document.getElementById('email').value === ''
            || document.getElementById('dateOfBirth').value === ''
            || document.getElementById('dateOfBirth').value === null
            || document.getElementById('cardNumber').value === '') {
            setFormState(formState => ({
                ...formState,
                isSaveValid: false
            }));
        } else {
            setFormState(formState => ({
                ...formState,
                isSaveValid: true
            }));
        }


    };

    const handleBack = () => {
        history.goBack();
    };
    

    const handleDelete = event => {
        event.preventDefault();

        setFormState(formState => ({
            ...formState,
            uvjetMessage: true
        }))


        if (SID != null) {
            axios.delete(`https://tecajevi-i-lekcije.herokuapp.com/customers/${SID}`)
                .then(res => {
                    console.log(res);
                    console.log(res.data);
                }).catch(e => {
                console.log(e);
            });
        }

        window.location.href = '/customerTable';

    }

    const handleSave = event => {
        event.preventDefault();

        setFormState(formState => ({
            ...formState,
            uvjetMessage: true
        }));

        const dateString = document.getElementById('dateOfBirth').value.toString();
        const arrayDate = dateString.split("-");
        const newFormatDate = arrayDate[2] + "-" + arrayDate[1] + "-" + arrayDate[0];

        if (SID == null) {

            const param = {
                id: null,
                username: document.getElementById('username').value,
                password: document.getElementById('password').value,
                firstName: document.getElementById('firstName').value,
                lastName: document.getElementById('lastName').value,
                email: document.getElementById('email').value,
                dateOfBirth: document.getElementById('dateOfBirth').value,
                cardNumber: document.getElementById('cardNumber').value
            };
            console.log(param);
            axios.post('https://tecajevi-i-lekcije.herokuapp.com/customers', param)
                .then(res => {
                    console.log(res);
                }).catch(e => {
                console.log(e);
            });
            window.location.href = '/customerTable';
            return;
        }

        if (SID != null) {
            const param2 = {
                id: SID,
                username: document.getElementById('username').value,
                password: document.getElementById('password').value,
                firstName: document.getElementById('firstName').value,
                lastName: document.getElementById('lastName').value,
                email: document.getElementById('email').value,
                dateOfBirth: document.getElementById('dateOfBirth').value,
                cardNumber: document.getElementById('cardNumber').value
            };

            axios.put(`https://tecajevi-i-lekcije.herokuapp.com/customers/${SID}`, param2)
                .then(res => {
                    console.log(res);
                    console.log(res.data);
                }).catch(e => {
                console.log(e);
            });
            window.location.href = '/customerTable';
            return;
        }
        window.location.href = '/customerTable';
    };

    const hasError = field =>
        formState.touched[field] && formState.errors[field] ? true : false;

    return (
        <div className={classes.root}>
            <Grid
                className={classes.grid}
                container
            >
                <Grid
                    className={classes.content}
                    item
                    lg={7}
                    xs={12}
                >
                    <div className={classes.content}>
                        <div className={classes.contentHeader}>
                            <IconButton onClick={handleBack}>
                                <ArrowBackIcon/>
                            </IconButton>
                        </div>
                        <div className={classes.contentBody}>
                            <form
                                className={classes.form}
                            >
                                <Typography
                                    className={classes.title}
                                    variant="h2"
                                >
                                    Customer Management
                                </Typography>
                                <Typography
                                    color="textSecondary"
                                    gutterBottom
                                >
                                </Typography>
                                <TextField
                                    className={classes.textField}
                                    id="username"
                                    error={hasError('username')}
                                    fullWidth
                                    helperText={
                                        hasError('username') ? formState.errors.username[0] : null
                                    }
                                    label="Username"
                                    name="username"
                                    onChange={handleChange}
                                    type="text"
                                    value={formState.values.username || ''}
                                    variant="outlined"
                                />
                                <TextField
                                    className={classes.textField}
                                    id="password"
                                    error={hasError('password')}
                                    fullWidth
                                    helperText={
                                        hasError('password') ? formState.errors.password[0] : null
                                    }
                                    label="Password"
                                    name="password"
                                    onChange={handleChange}
                                    type="password"
                                    value={formState.values.password || ''}
                                    variant="outlined"
                                />
                                <TextField
                                    className={classes.textField}
                                    id="firstName"
                                    error={hasError('firstName')}
                                    fullWidth
                                    helperText={
                                        hasError('firstName') ? formState.errors.firstName[0] : null
                                    }
                                    label="First Name"
                                    name="firstName"
                                    onChange={handleChange}
                                    type="text"
                                    value={formState.values.firstName || ''}
                                    variant="outlined"
                                />
                                <TextField
                                    className={classes.textField}
                                    id="lastName"
                                    error={hasError('lastName')}
                                    fullWidth
                                    helperText={
                                        hasError('lastName') ? formState.errors.lastName[0] : null
                                    }
                                    label="Last Name"
                                    name="lastName"
                                    onChange={handleChange}
                                    type="text"
                                    value={formState.values.lastName || ''}
                                    variant="outlined"
                                />
                                <TextField
                                    className={classes.textField}
                                    error={hasError('email')}
                                    id="email"
                                    fullWidth
                                    helperText={
                                        hasError('email') ? formState.errors.email[0] : null
                                    }
                                    label="Email adress"
                                    name="email"
                                    onChange={handleChange}
                                    type="email"
                                    value={formState.values.email || ''}
                                    variant="outlined"
                                />
                                <InputLabel
                                    id="demo-simple-select-label"
                                    className={classes.inputLabel}
                                >
                                    Date of Birth:
                                </InputLabel>
                                <TextField
                                    className={classes.textField}
                                    id="dateOfBirth"
                                    error={hasError('dateOfBirth')}
                                    fullWidth
                                    helperText={
                                        hasError('dateOfBirth') ? formState.errors.dateOfBirth[0] : null
                                    }
                                    name="dateOfBirth"
                                    onChange={handleChange}
                                    type="date"
                                    value={formState.values.dateOfBirth || ''}
                                    variant="outlined"
                                /> 
                               <TextField
                                    className={classes.textField}
                                    id="cardNumber"
                                    error={hasError('cardNumber')}
                                    fullWidth
                                    helperText={
                                        hasError('cardNumber') ? formState.errors.cardNumber[0] : null
                                    }
                                    name="cardNumber"
                                    onChange={handleChange}
                                    type="text"
                                    value={formState.values.cardNumber || ''}
                                    variant="outlined"
                                /> 
                                <Button
                                    className={classes.ButtonDelete}
                                    id="deleteBtn"
                                    color="primary"
                                    disabled={!formState.isDeleteValid}
                                    size="large"
                                    type="submit"
                                    variant="contained"
                                    onClick={handleDelete}
                                >
                                    <Delete/>
                                    Delete
                                </Button>
                                <Button
                                    className={classes.Button}
                                    id="saveBtn"
                                    color="primary"
                                    disabled={!formState.isSaveValid}
                                    size="large"
                                    type="submit"
                                    variant="contained"
                                    onClick={handleSave}
                                >
                                    <Save/>
                                    Save
                                </Button>
                                <Messages uvjet={formState.uvjetMessage}/>
                            </form>
                        </div>
                    </div>
                </Grid>
            </Grid>
        </div>
    );
};

CustomerManagement.propTypes = {
    history: PropTypes.object
};

export default CustomerManagement;