import React, {useState, useEffect} from 'react';
import {Link as RouterLink, withRouter} from 'react-router-dom';
import PropTypes from 'prop-types';
import validate from 'validate.js';
import {makeStyles} from '@material-ui/core/styles';
import Delete from '@material-ui/icons/Delete';
import Save from '@material-ui/icons/Save';
import Create from '@material-ui/icons/Create';
import Messages from './Messages/Messages';
import {
    Grid,
    Button,
    Input,
    InputLabel,
    IconButton,
    Select,
    TextField,
    Link,
    FormHelperText,
    Checkbox,
    MenuList,
    MenuItem,
    Typography
} from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import axios from 'axios';

const schema = {
    serviceId: {
        presence: {allowEmpty: true, message: 'is required'},
        length: {
            maximum: 32
        }
    },
    name: {
        presence: {allowEmpty: false, message: 'is required'},
        length: {
            maximum: 32
        }
    },
    surname: {
        presence: {allowEmpty: false, message: 'is required'},
        length: {
            maximum: 32
        }
    },
    group: {
        presence: {allowEmpty: false, message: 'is required'},
        length: {
            maximum: 32
        }
    },
    serviceDescription: {
        presence: {allowEmpty: false, message: 'is required'},
        length: {
            maximum: 256
        }
    },
    leader: {
        presence: {allowEmpty: false, message: 'is required'},
        length: {
            maximum: 32
        }
    },
    pricePerHour: {
        presence: {allowEmpty: false, message: 'is required'},
    }
};

const useStyles = makeStyles(theme => ({
    root: {
        backgroundColor: theme.palette.background.default,
        height: '100%'
    },
    grid: {
        height: '100%'
    },
    name: {
        marginTop: theme.spacing(3),
        color: theme.palette.white
    },
    bio: {
        color: theme.palette.white
    },
    contentContainer: {},
    content: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column'
    },
    contentHeader: {
        display: 'flex',
        alignItems: 'center',
        paddingTop: theme.spacing(5),
        paddingBottom: theme.spacing(2),
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2)
    },
    logoImage: {
        marginLeft: theme.spacing(4)
    },
    contentBody: {
        flexGrow: 1,
        display: 'flex',
        alignItems: 'center',
    },
    form: {
        paddingLeft: 100,
        paddingRight: 100,
        paddingBottom: 125,
        flexBasis: 700,
    },
    title: {
        marginTop: theme.spacing(3)
    },
    textField: {
        marginTop: theme.spacing(2)
    },
    inputLabel: {
        marginTop: theme.spacing(4)
    },
    policy: {
        marginTop: theme.spacing(1),
        display: 'flex',
        alignItems: 'center'
    },
    policyCheckbox: {
        marginLeft: '-14px'
    },
    Button: {
        margin: theme.spacing(2, 0),
        width: 100
    },
    ButtonDelete: {
        margin: theme.spacing(2, 0),
        width: 100,
        marginRight: 30,
        backgroundColor: 'red'
    }
}));

const PaymentManagement = props => {
    const {history} = props;

    const helpSID = window.localStorage.getItem("paymentId");
    let SID = helpSID == "null" ? null : helpSID;

    const classes = useStyles();

    const [formState, setFormState] = useState({
        isValid: false,
        isSaveValid: false,
        isDeleteValid: false,
        isCustomerSelected: false,
        isCourseSelected: false,
        isPaymentTypeSelected: false,
        uvjetMessage: false,
        vrijednosti: [],
        values: {
            course: '',
            discount: 0,
            customer: '',
            price: 0,
            paymentType: '',
            discountPrice: 0,
        },
        touched: {},
        errors: {},
        customers: [],
        courses: [],
        payments: [],
        paymentTypes:[],
        selectedCustomer: {},
        selectedCourse: {},
        selectedPaymentType: {}
    });

    let isNull = true;

    useEffect(() => {

        console.log(SID);
        axios.get("https://tecajevi-i-lekcije.herokuapp.com/payments")
            .then(res => {
                res.data.map(payment => {
                    if(payment.id == SID){
                        axios.get("https://tecajevi-i-lekcije.herokuapp.com/customers")
                        .then(response => {
                            response.data.map(customer => {
                                if (customer.id === payment.customerId) {
                                    axios.get("https://tecajevi-i-lekcije.herokuapp.com/courses")
                                    .then(rep=>{
                                        rep.data.map(course=>{
                                            if(course.id === payment.courseId){
                                                axios.get("https://tecajevi-i-lekcije.herokuapp.com/paymentTypes")
                                                .then(r=>{
                                                    r.data.map(paymentType =>{
                                                        if(paymentType.id === payment.paymentTypeId){
                                                            setFormState(formState => ({
                                                                ...formState,
                                                                values: {
                                                                    ...formState.values,
                                                                    course: course.name,
                                                                    paymentType: paymentType.name,
                                                                    price: payment.price,
                                                                    customer: customer.username,
                                                                    discount:payment.discount,
                                                                    discountPrice: payment.price - payment.price * payment.discount / 100
                                                                }
                                                            }));
                                                        }
                                                    })
                                                })
                                            }
                                        })
                                    })
                                                    
                                    }
                                });
                        });
                    }
                    
                                })
                                setFormState(formState => ({
                                    ...formState,
                                    vrijednosti: res.data,
                                    payments: res.data,
                                }))
                            });

                
            

        axios.get("https://tecajevi-i-lekcije.herokuapp.com/customers")
            .then(res => {

                setFormState(formState => ({
                    ...formState,
                    customers: res.data
                }))
            });
            axios.get("https://tecajevi-i-lekcije.herokuapp.com/paymentTypes")
            .then(res => {

                setFormState(formState => ({
                    ...formState,
                    paymentTypes: res.data
                }))
            });
            axios.get("https://tecajevi-i-lekcije.herokuapp.com/courses")
            .then(res => {

                setFormState(formState => ({
                    ...formState,
                    courses: res.data
                }))
            });


        if (SID != null) {
            setFormState(formState => ({
                ...formState,
                isSaveValid: false,
                isDeleteValid: true,
                isCustomerSelected: false,
                isCourseSelected: false,
                isPaymentTypeSelected: false,
            }));
        }
    }, []);


    const handleChange = event => {
        event.persist();

        setFormState(formState => ({
            ...formState,
            values: {
                ...formState.values,
                [event.target.name]:
                    event.target.type === 'checkbox'
                        ? event.target.checked
                        : event.target.value
            },
            touched: {
                ...formState.touched,
                [event.target.name]: true
            }
        }));


        if (formState.isCustomerSelected === false
            || formState.isCourseSelected === false
            || formState.isPaymentTypeSelected === false) {
            setFormState(formState => ({
                ...formState,
                isSaveValid: false
            }));
        } else {
            setFormState(formState => ({
                ...formState,
                isSaveValid: true
            }));
        }


    };


    const handleCustomer = event => {
        setFormState(formState => ({
            ...formState,
            isCustomerSelected: true,
            selectedCustomer: event.target.value,
            values: {
                ...formState.values,
                [event.target.name]:
                    event.target.type === 'checkbox'
                        ? event.target.checked
                        : event.target.value
            },
            touched: {
                ...formState.touched,
                [event.target.name]: true
            }
        }));

        if (formState.isCourseSelected === false
            || formState.isPaymentTypeSelected === false) {
            setFormState(formState => ({
                ...formState,
                isSaveValid: false
            }));
        } else {
            setFormState(formState => ({
                ...formState,
                isSaveValid: true
            }));
        }
    };

    const handleCourse = event => {
        setFormState(formState => ({
            ...formState,
            isCourseSelected: true,
            selectedCourse: event.target.value,
            values: {
                ...formState.values,
                [event.target.name]:
                    event.target.type === 'checkbox'
                        ? event.target.checked
                        : event.target.value
            },
            touched: {
                ...formState.touched,
                [event.target.name]: true
            }
        }));

        if (formState.isCustomerSelected === false
            || formState.isPaymentTypeSelected === false) {
            setFormState(formState => ({
                ...formState,
                isSaveValid: false
            }));
        } else {
            setFormState(formState => ({
                ...formState,
                isSaveValid: true
            }));
        }
    };

    const handlePaymentType = event => {
        setFormState(formState => ({
            ...formState,
            isPaymentTypeSelected: true,
            selectedPaymentType: event.target.value,
            values: {
                ...formState.values,
                [event.target.name]:
                    event.target.type === 'checkbox'
                        ? event.target.checked
                        : event.target.value
            },
            touched: {
                ...formState.touched,
                [event.target.name]: true
            }
        }));

        if (formState.isCourseSelected === false
            || formState.isCustomerSelected === false) {
            setFormState(formState => ({
                ...formState,
                isSaveValid: false
            }));
        } else {
            setFormState(formState => ({
                ...formState,
                isSaveValid: true
            }));
        }
    };

    const handleBack = () => {
        history.goBack();
    };


    const handleDelete = event => {
        event.preventDefault();

        setFormState(formState => ({
            ...formState,
            uvjetMessage: true
        }));

        if (SID != null) {
            axios.delete(`https://tecajevi-i-lekcije.herokuapp.com/payments/${SID}`)
                .then(res => {
                    console.log(res);
                    console.log(res.data);
                }).catch(e => {
                console.log(e);
            });
        }

    }


    const handleSave = event => {
        event.preventDefault();

        setFormState(formState => ({
            ...formState,
            uvjetMessage: true
        }));

        let helpCustomerId = 99;
        let helpCourseId = 99;
        let helpPaymentTypeId = 99;
        let coursePrice = 0;

        
            formState.customers.forEach(current => {
                if (current.username === formState.selectedCustomer) {
                    helpCustomerId = current.id;
                }
            });
            
            formState.courses.forEach(current => {
                if (current.name === formState.selectedCourse) {
                    helpCourseId = current.id;
                    coursePrice = current.price;
                }
            });
            formState.paymentTypes.forEach(current => {
                if (current.name === formState.selectedPaymentType) {
                    helpPaymentTypeId = current.id;
                }
            });
        
        

        if (SID == null) {

            const param = {
                courseId: helpCourseId,
                paymentTypeId: helpPaymentTypeId,
                customerId: helpCustomerId,
                price: coursePrice
            };
            console.log(param);
            axios.post('https://tecajevi-i-lekcije.herokuapp.com/payments', param)
                .then(res => {
                    console.log(res);
                }).catch(e => {
                console.log(e);
            });
           
            return;
        }

        if (SID != null) {
            const param2 = {
                id: SID,
                courseId: helpCourseId,
                paymentTypeId: helpPaymentTypeId,
                customerId: helpCustomerId,
                price: coursePrice
            };
            console.log(param2);
            axios.put(`https://tecajevi-i-lekcije.herokuapp.com/payments/${SID}`, param2)
                .then(res => {
                    console.log(res);
                    console.log(res.data);
                }).catch(e => {
                console.log(e);
            });
            
           
        }
        
    };

    const hasError = field =>
        formState.touched[field] && formState.errors[field] ? true : false;

    return (
        <div className={classes.root}>
            <Grid
                className={classes.grid}
                container
            >
                <Grid
                    className={classes.content}
                    item
                    lg={7}
                    xs={12}
                >
                    <div className={classes.content}>
                        <div className={classes.contentHeader}>
                            <IconButton onClick={handleBack}>
                                <ArrowBackIcon/>
                            </IconButton>
                        </div>
                        <div className={classes.contentBody}>
                            <form
                                className={classes.form}
                            >
                                <Typography
                                    className={classes.title}
                                    variant="h2"
                                >
                                    Payment Management
                                </Typography>
                                <Typography
                                    color="textSecondary"
                                    gutterBottom
                                >
                                </Typography>
        
                                <InputLabel
                                    id="demo-simple-select-label"
                                    className={classes.inputLabel}
                                >
                                    Select a Customer:
                                </InputLabel>
                                <Select
                                    //className={classes.textField}
                                    labelId="demo-simple-select-label"
                                    id="customer"
                                    error={hasError('customer')}
                                    fullWidth
                                    helperText={
                                        hasError('customer') ? formState.errors.customer[0] : null
                                    }
                                    name="customer"
                                    onChange={handleCustomer}
                                    type="text"
                                    value={formState.values.customer}
                                    variant="outlined"
                                >
                                    {formState.customers.map(current => (
                                        <MenuItem
                                            value={current.username}
                                            id={current.id}
                                            onChange={handleChange}>
                                            {current.username}
                                        </MenuItem>
                                    ))};
                                </Select>
                                <InputLabel
                                    id="demo-simple-select-label"
                                    className={classes.inputLabel}
                                >
                                    Select a Course:
                                </InputLabel>
                                <Select
                                    //className={classes.textField}
                                    labelId="demo-simple-select-label"
                                    id="course"
                                    error={hasError('course')}
                                    fullWidth
                                    helperText={
                                        hasError('course') ? formState.errors.course[0] : null
                                    }
                                    name="course"
                                    onChange={handleCourse}
                                    type="text"
                                    value={formState.values.course}
                                    variant="outlined"
                                >
                                    {formState.courses.map(current => (
                                        <MenuItem
                                            value={current.name}
                                            id={current.id}
                                            onChange={handleChange}>
                                            {current.name}
                                        </MenuItem>
                                    ))};
                                </Select>
                                <InputLabel
                                    id="demo-simple-select-label"
                                    className={classes.inputLabel}
                                >
                                    Select a Payment Type:
                                </InputLabel>
                                <Select
                                    //className={classes.textField}
                                    labelId="demo-simple-select-label"
                                    id="paymentType"
                                    error={hasError('paymentType')}
                                    fullWidth
                                    helperText={
                                        hasError('paymentType') ? formState.errors.paymentType[0] : null
                                    }
                                    name="paymentType"
                                    onChange={handlePaymentType}
                                    type="text"
                                    value={formState.values.paymentType}
                                    variant="outlined"
                                >
                                    {formState.paymentTypes.map(current => (
                                        <MenuItem
                                            value={current.name}
                                            id={current.id}
                                            onChange={handleChange}>
                                            {current.name}
                                        </MenuItem>
                                    ))};
                                </Select>
                                <TextField
                                    className={classes.textField}
                                    error={hasError('discount')}
                                    id="discount"
                                    disabled = "true"
                                    fullWidth
                                    helperText={
                                        hasError('discount') ? formState.errors.discount[0] : null
                                    }
                                    label="Discount"
                                    name="discount"
                                    onChange={handleChange}
                                    type="number"
                                    InputProps={{ inputProps: { min: 0 } }}
                                    value={formState.values.discount || ''}
                                    variant="outlined"
                                />

                                <TextField
                                    className={classes.textField}
                                    error={hasError('price')}
                                    id="price"
                                    disabled = "true"
                                    fullWidth
                                    helperText={
                                        hasError('price') ? formState.errors.price[0] : null
                                    }
                                    label="Price (Original)"
                                    name="price"
                                    onChange={handleChange}
                                    type="number"
                                    InputProps={{ inputProps: { min: 0 } }}
                                    value={formState.values.price || ''}
                                    variant="outlined"
                                />

                                <TextField
                                    className={classes.textField}
                                    error={hasError('discountPrice')}
                                    id="discountPrice"
                                    disabled = "true"
                                    fullWidth
                                    helperText={
                                        hasError('discountPrice') ? formState.errors.discountPrice[0] : null
                                    }
                                    label="Price (Discount)"
                                    name="discountPrice"
                                    onChange={handleChange}
                                    type="number"
                                    InputProps={{ inputProps: { min: 0 } }}
                                    value={formState.values.discountPrice || ''}
                                    variant="outlined"
                                />
                               
                                <Button
                                    className={classes.ButtonDelete}
                                    id="deleteBtn"
                                    color="primary"
                                    disabled={!formState.isDeleteValid}
                                    size="large"
                                    type="submit"
                                    variant="contained"
                                    onClick={handleDelete}
                                >
                                    <Delete/>
                                    Delete
                                </Button>
                                <Button
                                    className={classes.Button}
                                    id="saveBtn"
                                    color="primary"
                                    disabled={!formState.isSaveValid}
                                    size="large"
                                    type="submit"
                                    variant="contained"
                                    onClick={handleSave}
                                >
                                    <Save/>
                                    Save
                                </Button>
                                <Messages uvjet={formState.uvjetMessage}/>
                            </form>
                        </div>
                    </div>
                </Grid>
            </Grid>
        </div>
    );
};

PaymentManagement.propTypes = {
    history: PropTypes.object
};

export default PaymentManagement;