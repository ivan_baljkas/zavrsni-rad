import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import CreateIcon from '@material-ui/icons/Create';
import {
    Grid,
    Input,
    Button,
    InputLabel,
    Fab,
    IconButton,
    TableContainer,
    Select,
    TextField,
    Link,
    FormHelperText,
    Checkbox,
    MenuList,
    MenuItem,
    Typography
} from '@material-ui/core';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import axios from 'axios';

const useStyles = makeStyles(theme => ({
    root: {
        backgroundColor: theme.palette.background.default,
        height: '100%'
    },
    grid: {
        height: '100%'
    },
    name: {
        marginTop: theme.spacing(3),
        color: theme.palette.white
    },
    bio: {
        color: theme.palette.white
    },
    contentContainer: {},
    content: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column'
    },
    contentHeader: {
        display: 'flex',
        alignItems: 'center',
        paddingTop: theme.spacing(5),
        paddingBottom: theme.spacing(2),
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2)
    },
    contentHeader2: {
        display: 'flex',
        alignItems: 'center',
        paddingTop: theme.spacing(1),
        paddingBottom: theme.spacing(2),
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2)
    },
    logoImage: {
        marginLeft: theme.spacing(4)
    },
    contentBody: {
        flexGrow: 1,
        display: 'flex',
        alignItems: 'center'
    },
    form: {
        paddingLeft: 100,
        paddingBottom: 125,
        flexBasis: 900,
    },
    title: {
        marginTop: theme.spacing(3)
    },
    textField: {
        marginTop: theme.spacing(2)
    },
    inputLabel: {
        marginTop: theme.spacing(4)
    },
    policy: {
        marginTop: theme.spacing(1),
        display: 'flex',
        alignItems: 'center'
    },
    policyCheckbox: {
        marginLeft: '-14px'
    },
    Button: {
         margin: theme.spacing(2, 0),
         width: 130
    },
    ButtonDelete: {
        margin: theme.spacing(2, 0),
        width: 100,
        marginRight: 30,
        backgroundColor: 'red'
    },
    Fab: {
        marginLeft: '10px'
    }
}));

const MemberTable = (props) =>{

    const { history } = props;
    const classes = useStyles();

    const helpSID2 = window.localStorage.getItem("courseId");
    let SID2 = helpSID2 == "null" ? null : helpSID2;

    const [formState, setFormState] = useState({
        isValid: false,
        services: [],
        values: {
            id: 0,
            firstName: "",
            lastName: "",
            username: "",
            email: ""
        },
        touched: {},
        uvjetMessage: false,
        errors: {},
        count: 0,
        customers:[],
        newMembers:[]
    });

    const handleCreate = (event) =>{
    event.preventDefault();

        //trebalo bi ga linkati na upravljanje djelatnostima bez poslanog propsa za id
        window.localStorage.setItem("customerId", null);
        window.location.href = '/customerManagement';
    }

    useEffect(() =>{

        axios.get("https://tecajevi-i-lekcije.herokuapp.com/courses")
            .then(res => {

                const helpCourses = res.data;
                    helpCourses.forEach(course => {
                        if (course.id == SID2) {
                           
                            axios.get("https://tecajevi-i-lekcije.herokuapp.com/customers")
                                .then(response => {
                                    const newCustomers = response.data;
                                    newCustomers.forEach(customer => {
                                        if (course.membersId.indexOf(customer.id) === -1) {
                                            setFormState(formState =>({
                                                ...formState,
                                                customers: [...formState.customers,
                                                    { 
                                                        id: customer.id,
                                                        firstName: customer.firstName,
                                                        lastName: customer.lastName,
                                                        username: customer.username,
                                                        email: customer.email}]
                                            }));
                                        }
                                        
                                    });
                                });
                           
                        }
                    });
                }
            );

    },[])

    const handleAdd = (rowId) =>{
        
        //trebalo bi ga linkati na ekran upravljanje djelatnostima sa propsom id koji ce biti jednak rowId argumentu funkcije

        let param = {};

        axios.get("https://tecajevi-i-lekcije.herokuapp.com/courses")
                                .then(response => {
                                    const newCourses = response.data;
                                    newCourses.forEach(course => {
                                        if (course.id == SID2) {
                                            let newMembers = [];

                                            for(var i=0; i<course.membersId.length; i++){
                                                newMembers.push(course.membersId[i]);
                                             }
                                             newMembers.push(rowId);

                                            param = {
                                                name: course.name,
                                                description: course.description,
                                                lecturerId: course.lecturerId,
                                                price: course.price,
                                                capacity: course.capacity,
                                                membersId : [...course.membersId,rowId]
                                            };
                                            console.log(param);
                                           
                                        }
                                        
                                    });
                                });

                                
                                
            axios.put(`https://tecajevi-i-lekcije.herokuapp.com/courses/${SID2}`, param)
                    .then(res => {
                         console.log(res);
                         console.log(res.data);
                    }).catch(e => {
                    console.log(e);
                    });
                                                  
                
        window.localStorage.setItem("courseId", SID2);
        props.history.push('/courseMemberTable');

    }


    const handleChange = (event) =>{
    event.persist();

    }


    const handleBack = () => {
        window.location.href = '/';
    };

    return(
        <div className={classes.root}>
            <Grid
                className={classes.grid}
                container
            >
                <Grid
                    className={classes.content}
                    item
                    lg={7}
                    xs={12}
                >
                    <div className={classes.content}>
                        <div className={classes.contentHeader}>
                            <IconButton onClick={handleBack}>
                                <ArrowBackIcon />
                            </IconButton>
                        </div>
                        <div className={classes.contentBody}>
                            <form
                                className={classes.form}
                                //onSubmit={handleSignUp}
                            >
                                <Typography
                                    className={classes.title}
                                    variant="h2"
                                >
                                    Add New Members
                                </Typography>
                                <Typography
                                    color="textSecondary"
                                    gutterBottom
                                >
                                </Typography>
                                
                            <Paper>
                                <Table>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>Id</TableCell>
                                            <TableCell align="left">First Name</TableCell>
                                            <TableCell align="left">Last Name</TableCell>
                                            <TableCell align="left">Username</TableCell>
                                            <TableCell align="left">Email</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {formState.customers.map(row => (
                                            <TableRow key={row.id}>
                                                <TableCell component="th" scope="row">
                                                    {row.id}
                                                </TableCell>
                                                <TableCell align="left">{row.firstName}</TableCell>
                                                <TableCell align="left">{row.lastName}</TableCell>
                                                <TableCell align="left">{row.username}</TableCell>
                                                <TableCell align="left">{row.email}</TableCell>
                                                <TableCell align="right" onClick={() => handleAdd(row.id)}>
                                                        <Button
                                                            className={classes.Button}
                                                            id="detailsBtn"
                                                            color="primary"
                                                            size="large"
                                                            type="submit"
                                                            variant="contained"
                                                        >
                                                            Add
                                                        </Button>
                                                    </TableCell>

                                            </TableRow>
                                        ))}
                                    </TableBody>
                                </Table>
                            </Paper>
                            </form>
                        </div>
                    </div>
                </Grid>
            </Grid>
        </div>
    )

}

MemberTable.propTypes = {
    history: PropTypes.object
};

export default MemberTable;