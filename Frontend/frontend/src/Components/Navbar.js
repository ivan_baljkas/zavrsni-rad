import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from '@material-ui/styles';
import Link from "@material-ui/core/Link";
import MenuIcon from '@material-ui/icons/Menu';
import { Hidden, IconButton } from '@material-ui/core';
import {Button} from "react-bootstrap";

const useStyles = makeStyles(theme => ({
    root: {
        boxShadow: 'none'
    },
    flexGrow: {
        flexGrow: 1
    }
}));

const theme = {
    spacing: 8,
}

function LoginLink() {
    if((sessionStorage.getItem('isLogged') == 'false') || (sessionStorage.getItem('isLogged') == null)){
        return (<Link href={"/login"} color={"inherit"}>
            Login
        </Link>);
    }
    else{
        return (<Link onClick={setLogOff} href={'/'} color={'inherit'}>
            Logout
        </Link>);
    }
}

function setLogOff() {
    sessionStorage.clear();
   // window.location.href = '/';

}

const Navbar = (props) => {
    const classes = useStyles();
    const { onSidebarOpen } = props;

    return (
        <div>
            <AppBar position={"static"}>
                <Toolbar>
                    <Hidden lgUp>
                        <IconButton
                            color="inherit"
                            onClick={onSidebarOpen}
                        >
                            <MenuIcon />
                        </IconButton>
                    </Hidden>
                    <Typography variant="subtitle1" color="inherit">
                        <Link href={"/"} color={"inherit"}>
                            Tecajevi i lekcije
                        </Link>
                    </Typography>
                    

                    <div className={classes.flexGrow} />
                    <span className={"AddRightMargin"}></span>
                    <Typography variant="subtitle1" color={"inherit"}>
                        <LoginLink></LoginLink>
                    </Typography>
                </Toolbar>
            </AppBar>
        </div>
    )
}

export default Navbar;