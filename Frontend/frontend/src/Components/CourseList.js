import React, {Component} from "react";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Course from './Course';
import axios from "axios";

class CourseList extends Component{
    state = {
        courses: [],
        searchString: ''
    }
    

    constructor(){
        super()
        this.getCourses()
    }

    async getCourses (){
        const url = "https://tecajevi-i-lekcije.herokuapp.com/courses";
        axios.get(url).then((res) => {
            this.setState({courses : res.data})
            console.log(res.data);
            console.log(sessionStorage.getItem('username'),sessionStorage.getItem('isLogged'),sessionStorage.getItem('userID'));
        });
    }

    onSearchInputChange = (event) => {
        if (event.target.value) {
            this.setState({searchString: event.target.value})
            console.log(event.target.value);
        } else {
            this.setState({searchString: ''})
        }
    }

    render(){
        return (
            <div>
                {this.state.courses ? (
                    <div>
                        <TextField style={{padding: 24}}
                                   id={"searchInput"}
                                   placeholder={"Search for Courses"}
                                   margin={"normal"}
                                   inputProps={{min: 0, style: { textAlign: 'center' }}}
                                   onChange={this.onSearchInputChange}
                                   allign={"center"}/>
                        <Grid container spacing={1} style={{padding: 24}}>
                            {this.state.courses.filter(e => e.name.toLowerCase().includes(this.state.searchString.toLowerCase()))
                            .map(currentCourse => (
                                <Grid item xs={12} sm={6} lg={4} xl={3} key={currentCourse.id}>
                                   <Course course={currentCourse} />
                                </Grid>
                             ))}
                        </Grid>
                    </div>
                ) : "No courses found"}
            </div>
        )
    }
}

export default CourseList;